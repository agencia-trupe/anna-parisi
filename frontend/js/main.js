(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.mobileToggle = function(event) {
        event.preventDefault();

        var $handle = $(this),
            $nav    = $('nav#mobile');

        $nav.slideToggle();
        $handle.toggleClass('close');
    };

    App.homePreload = function() {
        var $preloader = $('.preloader');
        if (!$preloader.length) return;

        $('#slideshow').waitForImages({
            finished: function() {
                setTimeout(function() {
                   $preloader.fadeOut();
                }, 1000);
            },
            waitForAll: true
        });
    };

    App.homeCycle = function() {
        var $slideshow = $('#slideshow');
        if (!$slideshow.length) return;

        $slideshow.cycle({
            fx: 'fade',
            slides: '>div'
        });
    };

    App.gridMasonry = function() {
        var $grid = $('.grid');
        if (!$grid.length) return;

        $grid.waitForImages(function() {
            $grid.masonry({
                itemSelector: '.thumb-masonry',
                columnWidth: '.grid-sizer',
                gutter: '.gutter-sizer'
            });
        });
    };

    App.thumbFancybox = function() {
        var $grid = $('.grid');
        if (!$grid.length) return;

        $('.fancybox').fancybox({
            padding: 0,
            nextSpeed: 700,
            prevSpeed: 700,
            margin: [45, 45, 90, 45],
            beforeShow: function() {
                var navigationdiv  = '<div class="fancybox-customnav">';
                    navigationdiv += '<a href="#" class="fancybox-customprev">«</a>';
                    navigationdiv += '<span>' + (this.index + 1) + ' / ' + this.group.length + '</span>';
                    navigationdiv += '<a href="#" class="fancybox-customnext">»</a>';
                    navigationdiv += '</div>';

                $('.fancybox-outer').append(navigationdiv);
            }
        });

        $('body').on('click', 'a.fancybox-customprev', function() {
            $.fancybox.prev();
        });

        $('body').on('click', 'a.fancybox-customnext', function() {
            $.fancybox.next();
        });

        $('.thumb').click(function(e) {
            var el, id = $(this).data('galeria');
            if(id){
                el = $('.fancybox[rel=galeria' + id + ']:eq(0)');
                e.preventDefault();
                el.click();
            }
        });
    };

    App.thumbVideo = function() {
        $('.video').fancybox({
            padding: 0,
            type: 'iframe',
            width: 800,
            height: 450,
            aspectRatio: true
        });
    };

    App.thumbPhotoswipe = function() {
        var $grid = $('.grid, .imprensa');
        if (!$grid.length) return;

        $('.thumb').click(function(e) {
            var id = $(this).data('galeria');
            if(id){
                var pswpElement = document.querySelectorAll('.pswp')[0];
                var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, galerias[id], {
                    shareEl: false,
                    fullscreenEl: false
                });
                gallery.init();

                if($('.projetos-wrapper').length) {
                    gallery.listen('gettingData', function() {
                        $('.pswp__img').attr('title', 'Projeto ANNA PARISI Arquitetura+Design');
                    });
                }
            }
        });
    };

    App.contatoEnvio = function(event) {
        event.preventDefault();

        var $formContato = $(this);
        var $formContatoSubmit = $formContato.find('input[type=submit]');
        var $formContatoResposta = $formContato.find('#form-resposta');

        $formContatoResposta.hide();

        $.post(BASE + '/contato', {

            nome     : $('#nome').val(),
            email    : $('#email').val(),
            mensagem : $('#mensagem').val()

        }, function(data) {

            if (data.status == 'success') $formContato[0].reset();
            $formContatoResposta.hide().text(data.message).fadeIn('slow');

        }, 'json');
    };

    App.init = function() {
        App.homePreload();
        App.homeCycle();
        App.gridMasonry();
        App.thumbVideo();
        // App.thumbFancybox();
        App.thumbPhotoswipe();
        $('#form-contato').on('submit', App.contatoEnvio);
        $('#mobile-toggle').on('click touchstart', App.mobileToggle);
    };

    $(document).ready(App.init);

}(window, document, jQuery));
