<?php

// Front-end

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::get('perfil', ['as' => 'perfil', 'uses' => 'PerfilController@index']);
Route::get('projetos', ['as' => 'projetos', 'uses' => 'ProjetosController@index']);
Route::get('mostras', ['as' => 'mostras', 'uses' => 'MostrasController@index']);
Route::get('imprensa', ['as' => 'imprensa', 'uses' => 'ImprensaController@index']);
Route::get('contato', ['as' => 'contato', 'uses' => 'ContatoController@index']);
Route::post('contato', ['as' => 'contato.envio', 'uses' => 'ContatoController@envio']);


// Painel

Route::get('painel', [
    'before' => 'auth',
    'as'     => 'painel.home',
    'uses'   => 'Painel\HomeController@index'
]);

Route::get('painel/login', [
    'as'   => 'painel.login',
    'uses' => 'Painel\HomeController@login'
]);
Route::post('painel/login', [
    'as'   => 'painel.auth',
    'uses' => 'Painel\HomeController@attempt'
]);
Route::get('painel/logout', [
    'as'   => 'painel.logout',
    'uses' => 'Painel\HomeController@logout'
]);

Route::group(['prefix' => 'painel', 'before' => 'auth'], function() {
    Route::resource('banners', 'Painel\BannersController');

    Route::resource('perfil', 'Painel\PerfilController');

    Route::resource('projetos', 'Painel\ProjetosController');
    Route::resource('projetos.imagens', 'Painel\ProjetosImagensController');

    Route::resource('mostras', 'Painel\MostrasController');
    Route::resource('mostras.imagens', 'Painel\MostrasImagensController');

    Route::get('imprensa/delete-image/{imprensa_id}', [
        'as'   => 'painel.imprensa.deleteImage',
        'uses' => 'Painel\ImprensaController@deleteImage'
    ]);
    Route::get('imprensa/delete-pdf/{imprensa_id}', [
        'as'   => 'painel.imprensa.deletePdf',
        'uses' => 'Painel\ImprensaController@deletePdf'
    ]);
    Route::resource('imprensa', 'Painel\ImprensaController');
    Route::resource('imprensa.imagens', 'Painel\ImprensaImagensController');

    Route::resource('contato/recebidos', 'Painel\ContatosRecebidosController');
    Route::resource('contato', 'Painel\ContatoController');

    Route::resource('usuarios', 'Painel\UsuariosController');

    Route::post('ajax/order', 'Painel\AjaxController@order');
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});
