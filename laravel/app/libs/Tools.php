<?php

class Tools
{

    public static function formataData($data = '')
    {
        $meses = array(
            '01' => 'janeiro',
            '02' => 'fevereiro',
            '03' => 'março',
            '04' => 'abril',
            '05' => 'maio',
            '06' => 'junho',
            '07' => 'julho',
            '08' => 'agosto',
            '09' => 'setembro',
            '10' => 'outubro',
            '11' => 'novembro',
            '12' => 'dezembro'
        );

        if($data != '') {
            list($mes, $ano) = explode('/', $data);
            return $meses[$mes].' / '.$ano;
        } else {
            return $data;
        }
    }

    public static function videoThumb($tipo, $id)
    {
        try {
            if ($tipo == 'vimeo') {

                $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$id.php"));
                $thumbURL = isset($hash[0]['thumbnail_large']) ? $hash[0]['thumbnail_large'] : false;

                $filename = $id.'_'.date('YmdHis').'.jpg';
                copy($thumbURL, 'assets/img/videos/'.$filename);

                return $filename;

            } elseif ($tipo == 'youtube') {

                $thumbURL = "http://img.youtube.com/vi/$id/mqdefault.jpg";

                $filename = $id.'_'.date('YmdHis').'.jpg';
                copy($thumbURL, 'assets/img/videos/'.$filename);

                return $filename;

            }
        } catch (\Exception $e) {
            throw new \Exception('Erro ao obter imagem de capa, verifique o código do vídeo.', 1);
        }

        return '';
    }
}
