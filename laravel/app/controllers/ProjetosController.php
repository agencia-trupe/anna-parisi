<?php

use \Projeto;

class ProjetosController extends BaseController {

    public function index()
    {
        $projetos = Projeto::with('imagens')->ordenados()->get();
        return $this->view('frontend.projetos', compact('projetos'));
    }

}
