<?php

use \Perfil;

class PerfilController extends BaseController {

    public function index()
    {
        $perfil = Perfil::first();
        return $this->view('frontend.perfil', compact('perfil'));
    }

}
