<?php

use \Mostra;

class MostrasController extends BaseController {

    public function index()
    {
        $mostras = Mostra::with('imagens')->ordenados()->get();
        return $this->view('frontend.mostras', compact('mostras'));
    }

}
