<?php

namespace Painel;

use \Imprensa, \View, \Input, \Session, \Redirect, \Validator, \CropImage;

class ImprensaController extends BasePainelController {

    private $validation_rules = [
        'titulo' => 'required',
        'capa'   => 'image',
        'pdf'    => 'mimes:pdf'
    ];

    private $image_config = [
        'width'  => 200,
        'height' => 200,
        'bg'     => '#E6E6E6',
        'path'   => 'assets/img/imprensa/capa/'
    ];

    public function index()
    {
        $imprensa = Imprensa::ordenados()->get();

        return $this->view('painel.imprensa.index', compact('imprensa'));
    }

    public function create()
    {
        return $this->view('painel.imprensa.create');
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            if (Input::hasFile('capa')) {
                $input['capa'] = CropImage::make('capa', $this->image_config);
            } else {
                unset($input['capa']);
            }

            if (Input::has('video_codigo')) {
                $input['video_capa'] = \Tools::videoThumb($input['video_tipo'], $input['video_codigo']);
            }

            if (Input::hasFile('pdf')) {
                $input['pdf'] = $this->uploadPdf();
            }

            Imprensa::create($input);
            Session::flash('sucesso', 'Registro criado com sucesso.');

            return Redirect::route('painel.imprensa.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao criar registro: '.$e->getMessage()])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $imprensa = Imprensa::findOrFail($id);

        return $this->view('painel.imprensa.edit', compact('imprensa'));
    }

    public function update($id)
    {
        $imprensa = Imprensa::findOrFail($id);
        $input    = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            if (Input::hasFile('capa')) {
                $input['capa'] = CropImage::make('capa', $this->image_config);
            } else {
                unset($input['capa']);
            }

            if (Input::has('video_codigo') && $input['video_codigo'] != $imprensa->video_codigo || $input['video_tipo'] != $imprensa->video_tipo) {
                $input['video_capa'] = \Tools::videoThumb($input['video_tipo'], $input['video_codigo']);
            }

            if (Input::hasFile('pdf')) {
                $input['pdf'] = $this->uploadPdf();
            }

            $imprensa->update($input);
            Session::flash('sucesso', 'Registro alterado com sucesso.');

            return Redirect::route('painel.imprensa.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar registro: '.$e->getMessage()])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            Imprensa::destroy($id);
            Session::flash('sucesso', 'Registro removido com sucesso.');

            return Redirect::route('painel.imprensa.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover registro.']);

        }
    }

    public function deleteImage($id)
    {
        try {

            $imprensa = Imprensa::find($id);
            $imprensa->capa = null;
            $imprensa->save();

            Session::flash('sucesso', 'Imagem excluída com sucesso.');
            return Redirect::back();

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao excluir imagem.']);

        }
    }

    public function uploadPdf() {
        $file = Input::file('pdf');

        $path = 'assets/pdfs/';
        $name = preg_replace("([^\w\s\d\-_~,;:\[\]\(\).])", '', $file->getClientOriginalName());
        $name = date('YmdHis').'_'.$name;

        $file->move($path, $name);

        return $name;
    }

    public function deletePdf($id)
    {
        try {

            $imprensa = Imprensa::find($id);
            $imprensa->pdf = null;
            $imprensa->save();

            Session::flash('sucesso', 'Arquivo PDF excluído com sucesso.');
            return Redirect::back();

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao excluir PDF.']);

        }
    }
}
