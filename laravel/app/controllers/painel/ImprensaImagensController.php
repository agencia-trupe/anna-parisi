<?php

namespace Painel;

use \Imprensa, \ImprensaImagem, \View, \Input, \Session, \Redirect, \Validator, \CropImage;

class ImprensaImagensController extends BasePainelController {

    private $validation_rules = [
        'imagem'   => 'required|image'
    ];

    private $image_config = [
        'width'  => 3000,
        'height' => null,
        'upsize' => true,
        'path'   => 'assets/img/imprensa/'
    ];

    public function index($imprensa_id)
    {
        $imprensa = Imprensa::find($imprensa_id);
        if(!$imprensa) return Redirect::route('painel.imprensa.index');

        $imagens = ImprensaImagem::imprensa($imprensa_id)->ordenados()->get();

        return $this->view('painel.imprensaimagens.index', compact('imprensa', 'imagens'));
    }

    public function create($imprensa_id)
    {
        $imprensa = Imprensa::find($imprensa_id);
        if(!$imprensa) return Redirect::route('painel.imprensa.index');

        return $this->view('painel.imprensaimagens.create', compact('imprensa'));
    }

    public function store($imprensa_id)
    {
        $input = Input::all();
        $input['imprensa_id'] = $imprensa_id;

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['imagem'] = CropImage::make('imagem', $this->image_config);
            $imagem = ImprensaImagem::create($input);

            $imagem->largura = getimagesize(asset('assets/img/imprensa/'.rawurlencode($input['imagem'])))[0];
            $imagem->altura = getimagesize(asset('assets/img/imprensa/'.rawurlencode($input['imagem'])))[1];
            $imagem->save();

            Session::flash('sucesso', 'Imagem inserida com sucesso.');

            return Redirect::route('painel.imprensa.imagens.index', $input['imprensa_id']);

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao inserir imagem.'])
                ->withInput();

        }
    }

    public function destroy($imprensa_id, $imagem_id)
    {
        try {

            ImprensaImagem::destroy($imagem_id);
            Session::flash('sucesso', 'Imagem removida com sucesso.');

            return Redirect::route('painel.imprensa.imagens.index', $imprensa_id);

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover imagem.']);

        }
    }

}
