<?php

namespace Painel;

use \Projeto, \ProjetoImagem, \View, \Input, \Session, \Redirect, \Validator, \CropImage;

class ProjetosImagensController extends BasePainelController {

    private $validation_rules = [
        'imagem'   => 'required|image'
    ];

    private $image_config = [
        'width'  => 3000,
        'height' => null,
        'upsize' => true,
        'path'   => 'assets/img/projetos/'
    ];

    public function index($projetos_id)
    {
        $projeto = Projeto::find($projetos_id);
        if(!$projeto) return Redirect::route('painel.projetos.index');

        $imagens = ProjetoImagem::projeto($projetos_id)->ordenados()->get();

        return $this->view('painel.projetosimagens.index', compact('projeto', 'imagens'));
    }

    public function create($projetos_id)
    {
        $projeto = Projeto::find($projetos_id);
        if(!$projeto) return Redirect::route('painel.projetos.index');

        return $this->view('painel.projetosimagens.create', compact('projeto'));
    }

    public function store($projetos_id)
    {
        $input = Input::all();
        $input['projetos_id'] = $projetos_id;

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['imagem'] = CropImage::make('imagem', $this->image_config);
            $imagem = ProjetoImagem::create($input);

            $imagem->largura = getimagesize(asset('assets/img/projetos/'.rawurlencode($input['imagem'])))[0];
            $imagem->altura = getimagesize(asset('assets/img/projetos/'.rawurlencode($input['imagem'])))[1];
            $imagem->save();

            Session::flash('sucesso', 'Imagem inserida com sucesso.');

            return Redirect::route('painel.projetos.imagens.index', $input['projetos_id']);

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao inserir imagem.'])
                ->withInput();

        }
    }

    public function destroy($projeto_id, $imagem_id)
    {
        try {

            ProjetoImagem::destroy($imagem_id);
            Session::flash('sucesso', 'Imagem removida com sucesso.');

            return Redirect::route('painel.projetos.imagens.index', $projeto_id);

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover imagem.']);

        }
    }

}
