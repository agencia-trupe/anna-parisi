<?php

namespace Painel;

use \Mostra, \MostraImagem, \View, \Input, \Session, \Redirect, \Validator, \CropImage;

class MostrasImagensController extends BasePainelController {

    private $validation_rules = [
        'imagem'   => 'required|image'
    ];

    private $image_config = [
        'width'  => 3000,
        'height' => null,
        'upsize' => true,
        'path'   => 'assets/img/mostras/'
    ];

    public function index($mostras_id)
    {
        $mostra = Mostra::find($mostras_id);
        if(!$mostra) return Redirect::route('painel.mostras.index');

        $imagens = MostraImagem::mostra($mostras_id)->ordenados()->get();

        return $this->view('painel.mostrasimagens.index', compact('mostra', 'imagens'));
    }

    public function create($mostras_id)
    {
        $mostra = Mostra::find($mostras_id);
        if(!$mostra) return Redirect::route('painel.mostras.index');

        return $this->view('painel.mostrasimagens.create', compact('mostra'));
    }

    public function store($mostras_id)
    {
        $input = Input::all();
        $input['mostras_id'] = $mostras_id;

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['imagem'] = CropImage::make('imagem', $this->image_config);
            $imagem = MostraImagem::create($input);

            $imagem->largura = getimagesize(asset('assets/img/mostras/'.rawurlencode($input['imagem'])))[0];
            $imagem->altura = getimagesize(asset('assets/img/mostras/'.rawurlencode($input['imagem'])))[1];
            $imagem->save();

            Session::flash('sucesso', 'Imagem inserida com sucesso.');

            return Redirect::route('painel.mostras.imagens.index', $input['mostras_id']);

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao inserir imagem.'])
                ->withInput();

        }
    }

    public function destroy($mostra_id, $imagem_id)
    {
        try {

            MostraImagem::destroy($imagem_id);
            Session::flash('sucesso', 'Imagem removida com sucesso.');

            return Redirect::route('painel.mostras.imagens.index', $mostra_id);

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover imagem.']);

        }
    }

}
