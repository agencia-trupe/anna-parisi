<?php

namespace Painel;

use \Projeto, \View, \Input, \Session, \Redirect, \Validator, \CropImage;

class ProjetosController extends BasePainelController {

    private $validation_rules = [
        'titulo' => 'required',
        'capa'   => 'required|image'
    ];

    private $image_config = [
        'width'  => 300,
        'height' => null,
        'path'   => 'assets/img/projetos/capa/'
    ];

    public function index()
    {
        $projetos = Projeto::ordenados()->get();

        return $this->view('painel.projetos.index', compact('projetos'));
    }

    public function create()
    {
        return $this->view('painel.projetos.create');
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['capa'] = CropImage::make('capa', $this->image_config);
            Projeto::create($input);
            Session::flash('sucesso', 'Projeto criado com sucesso.');

            return Redirect::route('painel.projetos.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao criar projeto.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $projeto = Projeto::findOrFail($id);

        return $this->view('painel.projetos.edit', compact('projeto'));
    }

    public function update($id)
    {
        $projeto = Projeto::findOrFail($id);
        $input   = Input::all();

        $this->validation_rules['capa'] = 'image';
        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            if (Input::hasFile('capa')) {
                $input['capa'] = CropImage::make('capa', $this->image_config);
            } else {
                unset($input['capa']);
            }

            $projeto->update($input);
            Session::flash('sucesso', 'Projeto alterado com sucesso.');

            return Redirect::route('painel.projetos.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar projeto.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            Projeto::destroy($id);
            Session::flash('sucesso', 'Projeto removido com sucesso.');

            return Redirect::route('painel.projetos.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover projeto.']);

        }
    }

}
