<?php

namespace Painel;

use \Perfil, \Input, \Session, \Redirect, \Validator, \CropImage;

class PerfilController extends BasePainelController {

    private $validation_rules = [
        'texto' => 'required'
    ];

    public function index()
    {
    	$perfil = Perfil::first();

        return $this->view('painel.perfil.index', compact('perfil'));
    }

    public function update($id)
    {
        $perfil = Perfil::findOrFail($id);
        $input  = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            if (Input::hasFile('imagem')) {
                $input['imagem'] = CropImage::make('imagem', [
                    'width'  => 350,
                    'height' => null,
                    'path'   => 'assets/img/perfil/'
                ]);
            } else {
                unset($input['imagem']);
            }

            $perfil->update($input);
            Session::flash('sucesso', 'Página alterada com sucesso.');

            return Redirect::route('painel.perfil.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar página.'])
                ->withInput();

        }
    }

}
