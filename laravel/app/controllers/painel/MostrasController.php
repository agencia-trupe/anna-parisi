<?php

namespace Painel;

use \Mostra, \View, \Input, \Session, \Redirect, \Validator, \CropImage;

class MostrasController extends BasePainelController {

    private $validation_rules = [
        'titulo' => 'required',
        'capa'   => 'required|image'
    ];

    private $image_config = [
        'width'  => 300,
        'height' => null,
        'path'   => 'assets/img/mostras/capa/'
    ];

    public function index()
    {
        $mostras = Mostra::ordenados()->get();

        return $this->view('painel.mostras.index', compact('mostras'));
    }

    public function create()
    {
        return $this->view('painel.mostras.create');
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['capa'] = CropImage::make('capa', $this->image_config);
            Mostra::create($input);
            Session::flash('sucesso', 'Mostra criada com sucesso.');

            return Redirect::route('painel.mostras.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao criar mostra.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $mostra = Mostra::findOrFail($id);

        return $this->view('painel.mostras.edit', compact('mostra'));
    }

    public function update($id)
    {
        $mostra = Mostra::findOrFail($id);
        $input   = Input::all();

        $this->validation_rules['capa'] = 'image';
        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            if (Input::hasFile('capa')) {
                $input['capa'] = CropImage::make('capa', $this->image_config);
            } else {
                unset($input['capa']);
            }

            $mostra->update($input);
            Session::flash('sucesso', 'Mostra alterada com sucesso.');

            return Redirect::route('painel.mostras.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar mostra.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            Mostra::destroy($id);
            Session::flash('sucesso', 'Mostra removida com sucesso.');

            return Redirect::route('painel.mostras.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover mostra.']);

        }
    }

}
