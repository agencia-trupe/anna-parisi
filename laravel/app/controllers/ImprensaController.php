<?php

use \Imprensa;

class ImprensaController extends BaseController {

    public function index()
    {
        $imprensa = Imprensa::with('imagens')->ordenados()->get();
        return $this->view('frontend.imprensa', compact('imprensa'));
    }

}
