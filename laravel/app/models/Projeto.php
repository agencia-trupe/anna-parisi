<?php

class Projeto extends Eloquent
{

    protected $table = 'projetos';

    protected $hidden = [];

    protected $fillable = ['titulo', 'capa'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC');
    }

    public function imagens()
    {
        return $this->hasMany('ProjetoImagem', 'projetos_id')->orderBy('ordem', 'ASC');
    }

}
