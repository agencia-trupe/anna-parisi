<?php

class ProjetoImagem extends Eloquent
{

    protected $table = 'projetos_imagens';

    protected $hidden = [];

    protected $fillable = ['projetos_id', 'imagem'];

    public function scopeProjeto($query, $id)
    {
        return $query->where('projetos_id', $id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC');
    }

}
