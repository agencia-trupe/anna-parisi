<?php

class Contato extends Eloquent
{

    protected $table = 'contato';

    protected $hidden = [];

    protected $fillable = ['facebook', 'pinterest', 'instagram', 'telefones', 'email', 'endereco', 'googlemaps'];

}
