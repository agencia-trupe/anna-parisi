<?php

class Perfil extends Eloquent
{

    protected $table = 'perfil';

    protected $hidden = [];

    protected $fillable = ['texto', 'imagem'];

}
