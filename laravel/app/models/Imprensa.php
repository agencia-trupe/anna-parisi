<?php

class Imprensa extends Eloquent
{

    protected $table = 'imprensa';

    protected $hidden = [];

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('data', 'DESC');
    }

    public function imagens()
    {
        return $this->hasMany('ImprensaImagem', 'imprensa_id')->orderBy('ordem', 'ASC');
    }

    public function setDataAttribute($date)
    {
        $this->attributes['data'] = Carbon\Carbon::createFromFormat('m/Y', $date)->format('Y-m');
    }

    public function getDataAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m', $date)->format('m/Y');
    }

}
