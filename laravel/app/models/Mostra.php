<?php

class Mostra extends Eloquent
{

    protected $table = 'mostras';

    protected $hidden = [];

    protected $fillable = ['titulo', 'capa'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC');
    }

    public function imagens()
    {
        return $this->hasMany('MostraImagem', 'mostras_id')->orderBy('ordem', 'ASC');
    }

}
