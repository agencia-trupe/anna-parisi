<?php

class Banner extends Eloquent
{

    protected $table = 'banners';

    protected $hidden = [];

    protected $fillable = ['imagem'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC');
    }

}
