<?php

class MostraImagem extends Eloquent
{

    protected $table = 'mostras_imagens';

    protected $hidden = [];

    protected $fillable = ['mostras_id', 'imagem'];

    public function scopeMostra($query, $id)
    {
        return $query->where('mostras_id', $id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC');
    }

}
