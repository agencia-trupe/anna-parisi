<?php

class PerfilSeeder extends Seeder {

    public function run()
    {
        DB::table('perfil')->delete();

        $data = array(
            array(
                'texto' => 'Texto perfil.'
            )
        );

        DB::table('perfil')->insert($data);
    }

}
