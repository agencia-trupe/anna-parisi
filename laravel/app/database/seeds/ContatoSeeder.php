<?php

class ContatoSeeder extends Seeder {

    public function run()
    {
        DB::table('contato')->delete();

        $data = array(
            array(
                'telefones'  => '+55 11 98456 8470 | +55 11 2638 0782',
                'email'      => 'contato@annaparisi.com.br',
                'endereco'   => 'R. Jandiatuba 630 cj 528B - 05716-150 • São Paulo • SP',
                'googlemaps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.2018624777634!2d-46.7354086!3d-23.6329408!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5145ac6cc30b%3A0xe8484c79becef847!2sR.+Jandiatuba%2C+630+-+Vila+Andrade%2C+S%C3%A3o+Paulo+-+SP%2C+05716-150!5e0!3m2!1spt-BR!2sbr!4v1432834001344" width="600" height="450" frameborder="0" style="border:0"></iframe>',
                'facebook'   => 'https://www.facebook.com/arquiteturaannamariaparisi',
                'pinterest'  => 'https://www.pinterest.com/amparisi/',
                'instagram'  => 'https://instagram.com/annaparisi/'
            )
        );

        DB::table('contato')->insert($data);
    }

}
