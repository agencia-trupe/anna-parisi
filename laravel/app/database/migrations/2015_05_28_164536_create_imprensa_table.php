<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImprensaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('imprensa', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('data');
			$table->string('titulo');
			$table->string('capa')->nullable();
			$table->string('video_tipo');
			$table->string('video_codigo');
			$table->string('video_capa');
			$table->string('pdf')->nullable();
			$table->string('link');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('imprensa');
	}

}
