<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImprensaImagensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('imprensa_imagens', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('imprensa_id');
			$table->integer('ordem')->default(0);
			$table->string('largura');
			$table->string('altura');
			$table->string('imagem');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('imprensa_imagens');
	}

}
