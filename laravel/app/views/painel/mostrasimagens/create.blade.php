@section('content')

    <legend>
        <h2>Adicionar Imagem a Mostra: {{ $mostra->titulo }}</h2>
    </legend>

    {{ Form::open(['route' => ['painel.mostras.imagens.store', $mostra->id], 'files' => true]) }}

        @include('painel.mostrasimagens._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
