@section('content')

    <legend>
        <h2>Editar Projeto</h2>
    </legend>

    {{ Form::model($projeto, [
        'route' => ['painel.projetos.update', $projeto->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.projetos._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop
