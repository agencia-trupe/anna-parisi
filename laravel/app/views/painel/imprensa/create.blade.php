@section('content')

    <legend>
        <h2>Adicionar Imprensa</h2>
    </legend>

    {{ Form::open(['route' => 'painel.imprensa.store', 'files' => true]) }}

        @include('painel.imprensa._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
