@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="form-group">
    {{ Form::label('data', 'Data') }}
    {{ Form::text('data', null, ['class' => 'form-control', 'id' => 'datepicker']) }}
</div>

<div class="form-group">
    {{ Form::label('titulo', 'Título') }}
    {{ Form::text('titulo', null, ['class' => 'form-control']) }}
</div>

<div class="well form-group">
    {{ Form::label('capa', 'Imagem de capa') }}
@if($submitText == 'Alterar' && $imprensa->capa)
    <img src="{{ url('assets/img/imprensa/capa/'.$imprensa->capa) }}" style="display:block; margin-bottom: 10px; max-width:215px;height:auto;">
    <a href="{{ route('painel.imprensa.deleteImage', $imprensa->id) }}" class="btn-delete-link label label-danger" style="display:inline-block;margin-bottom:10px">
        <i class="glyphicon glyphicon-remove" style="margin-right:5px"></i>
        REMOVER
    </a>
@endif
    {{ Form::file('capa', ['class' => 'form-control']) }}
</div>

<hr>

<p class="alert alert-info">Preencha uma das colunas abaixo caso o conteúdo não seja uma galeria de imagens:</p>

<div class="row">
    <div class="col-md-4">
        <div class="well">
            <div class="form-group">
                {{ Form::label('video_tipo', 'Vídeo Tipo') }}
                {{ Form::select('video_tipo', ['youtube' => 'YouTube', 'vimeo' => 'Vimeo'], null, ['class' => 'form-control']) }}
            </div>
            <div class="form-group">
                {{ Form::label('video_codigo', 'Vídeo Código') }}
                {{ Form::text('video_codigo', null, ['class' => 'form-control']) }}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {{ Form::label('link', 'Link externo') }}
            {{ Form::text('link', null, ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {{ Form::label('pdf', 'Arquivo PDF') }}
            @if($submitText == 'Alterar' && $imprensa->pdf)
            <p>
                <a href="{{ url('assets/pdfs/'.$imprensa->pdf) }}" target="_blank" style="display:block;">{{ $imprensa->pdf }}</a>
                <a href="{{ route('painel.imprensa.deletePdf', $imprensa->id) }}" class="btn-delete-link label label-danger">
                    <i class="glyphicon glyphicon-remove" style="margin-right:5px"></i>
                    REMOVER
                </a>
            </p>
            @endif
            {{ Form::file('pdf', ['class' => 'form-control']) }}
        </div>
    </div>
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}

<a href="{{ route('painel.imprensa.index') }}" class="btn btn-default btn-voltar">Voltar</a>
