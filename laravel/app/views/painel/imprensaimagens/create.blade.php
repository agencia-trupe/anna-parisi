@section('content')

    <legend>
        <h2>Adicionar Imagem de Imprensa: {{ $imprensa->titulo }}</h2>
    </legend>

    {{ Form::open(['route' => ['painel.imprensa.imagens.store', $imprensa->id], 'files' => true]) }}

        @include('painel.imprensaimagens._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
