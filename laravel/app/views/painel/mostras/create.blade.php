@section('content')

    <legend>
        <h2>Adicionar Mostra</h2>
    </legend>

    {{ Form::open(['route' => 'painel.mostras.store', 'files' => true]) }}

        @include('painel.mostras._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
