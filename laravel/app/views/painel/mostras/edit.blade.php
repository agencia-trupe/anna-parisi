@section('content')

    <legend>
        <h2>Editar Mostra</h2>
    </legend>

    {{ Form::model($mostra, [
        'route' => ['painel.mostras.update', $mostra->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.mostras._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop
