@section('content')

    <legend>
        <h2>Adicionar Imagem ao Projeto: {{ $projeto->titulo }}</h2>
    </legend>

    {{ Form::open(['route' => ['painel.projetos.imagens.store', $projeto->id], 'files' => true]) }}

        @include('painel.projetosimagens._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
