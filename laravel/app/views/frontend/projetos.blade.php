@section('content')

    <div class="content help-mobile">
        <p>Clique para ver outras imagens</p>
    </div>

    <div class="content grid grid-projetos-mostras projetos-wrapper">
        <div class="gutter-sizer"></div>
        <div class="grid-sizer"></div>
        @foreach($projetos as $projeto)
        <a href="#" class="thumb thumb-masonry" data-galeria="{{ $projeto->id }}" title="{{ $projeto->titulo }}">
            <img src="{{ asset('assets/img/projetos/capa/'.$projeto->capa) }}" alt="" title="Projeto ANNA PARISI Arquitetura+Design">
            <div class="overlay">
                <span>{{ $projeto->titulo }}</span>
            </div>
        </a>
        @endforeach
    </div>

    {{-- <div class="hidden">
    @foreach($projetos as $projeto)
        @foreach($projeto->imagens as $imagem)
            <a href="{{ asset('assets/img/projetos/'.$imagem->imagem) }}" class="fancybox" rel="galeria{{ $projeto->id }}" title="{{ $projeto->titulo }}"></a>
        @endforeach
    @endforeach
    </div> --}}
    <script>
        var galerias = [];
        @foreach($projetos as $projeto)
            galerias[{{ $projeto->id }}] = [];
            @foreach($projeto->imagens as $imagem)
                galerias[{{ $projeto->id }}].push({
                    src: '{{ asset('assets/img/projetos/'.$imagem->imagem) }}',
                    title: '{{ $projeto->titulo }}',
                    w: {{ $imagem->largura }},
                    h: {{ $imagem->altura }},
                });
            @endforeach
        @endforeach
    </script>

@stop
