@section('content')

    <div class="content help-mobile">
        <p>Clique para ver outras imagens</p>
    </div>

    <div class="content imprensa">
        @foreach($imprensa as $registro)
            @if($registro->video_codigo)
                <a href="{{ $registro->video_tipo === 'youtube' ? 'https://youtube.com/embed/'.$registro->video_codigo.'?autoplay=1' : 'https://player.vimeo.com/video/'.$registro->video_codigo }}" class="imprensa-thumb video">
                    <div class="quadrado quadrado-video quadrado-imagem">
                        <div class="imagem" style="background-image:url('{{ asset('assets/img/videos/'.$registro->video_capa) }}')"></div>
                    </div>
                </a>
            @elseif($registro->pdf || $registro->link)
                <a href="{{ $registro->link ?: asset('assets/pdfs/'.$registro->pdf) }}" class="imprensa-thumb" target="_blank">
                    <div class="quadrado {{ $registro->link ? 'quadrado-link' : 'quadrado-pdf' }} {{ $registro->capa ? 'quadrado-imagem' : '' }}">
                        @if($registro->capa)
                        <div class="imagem" style="background-image:url('{{ asset('assets/img/imprensa/capa/'.$registro->capa) }}')"></div>
                        @else
                        <span>{{ $registro->titulo }}</span>
                        @endif
                    </div>
                </a>
            @else
                <a href="#" class="imprensa-thumb thumb" data-galeria="{{ $registro->id }}" title="{{ $registro->titulo }}">
                    <div class="quadrado">
                        @if($registro->capa)
                        <div class="imagem" style="background-image:url('{{ asset('assets/img/imprensa/capa/'.$registro->capa) }}')"></div>
                        @else
                        <span>{{ $registro->titulo }}</span>
                        @endif
                    </div>
                </a>
            @endif
        @endforeach
    </div>

    <script>
        var galerias = [];
        @foreach($imprensa as $registro)
            galerias[{{ $registro->id }}] = [];
            @foreach($registro->imagens as $imagem)
                galerias[{{ $registro->id }}].push({
                    src: '{{ asset('assets/img/imprensa/'.$imagem->imagem) }}',
                    title: '{{ $registro->titulo }}',
                    w: {{ $imagem->largura }},
                    h: {{ $imagem->altura }},
                });
            @endforeach
        @endforeach
    </script>

@stop
