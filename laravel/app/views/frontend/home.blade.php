@section('home')

    <div class="preloader">
        <div class="preloader-logo">
            <span></span>
        </div>
    </div>

    <div id="slideshow">
    @foreach($banners as $banner)
        <div style="background-image: url('assets/img/banners/{{ $banner->imagem }}')"></div>
    @endforeach
    </div>

@stop
