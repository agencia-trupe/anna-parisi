@section('content')

    <div class="content contato">
        <div class="inner">
            <p>
                <span>{{ $contato->telefones }}</span>
                <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a>
            </p>

            <form action="" method="post" id="form-contato">
                <input type="text" name="nome" id="nome" placeholder="Nome" required>
                <input type="email" name="email" id="email" placeholder="E-mail" required>
                <textarea name="mensagem" id="mensagem" placeholder="Mensagem" required></textarea>
                <input type="submit" value="Enviar">
                <div id="form-resposta"></div>
            </form>

            <p>{{ $contato->endereco }}</p>
        </div>
    </div>

@stop

@section('mapa')

    <div class="googlemaps">{{ $contato->googlemaps }}</div>
    <div class="copyright">
        <div class="center">
            <p class="left">© {{ date('Y') }} {{ Config::get('projeto.name') }} - Todos os direitos reservados.</p>
            <p class="right">
                <a href="http://www.trupe.net" target="blank">Criação de sites</a>:
                <a href="http://www.trupe.net" target="blank" class="trupe">Trupe Agência Criativa</a>
            </p>
        </div>
    </div>

@stop
