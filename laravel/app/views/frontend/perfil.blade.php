@section('content')

    <div class="content perfil">
        <div class="texto">
            <div class="logo"></div>
            <h2>Perfil</h2>
            {{ $perfil->texto }}
        </div>
    </div>

@stop

@section('home')

    <div class="perfil-bg" style="background-image:url({{ asset('assets/img/perfil/'.$perfil->imagem) }})"></div>

@stop
