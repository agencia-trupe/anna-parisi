    <div id="header-mobile">
        <h1><a href="{{ route('home') }}">{{ Config::get('projeto.title') }}</a></h1>
        <button id="mobile-toggle" type="button" role="button">
            <span class="lines"></span>
        </button>
    </div>

    <nav id="mobile">
        <ul class="menu">
            <li><a href="{{ route('perfil') }}" @if(str_is('perfil*', Route::currentRouteName())) class='active' @endif>Perfil</a></li>
            <li><a href="{{ route('projetos') }}" @if(str_is('projetos*', Route::currentRouteName())) class='active' @endif>Projetos</a></li>
            <li><a href="{{ route('mostras') }}" @if(str_is('mostras*', Route::currentRouteName())) class='active' @endif>Mostras</a></li>
            <li><a href="{{ route('imprensa') }}" @if(str_is('imprensa*', Route::currentRouteName())) class='active' @endif>Imprensa</a></li>
            <li><a href="{{ route('contato') }}" @if(str_is('contato*', Route::currentRouteName())) class='active' @endif>Contato</a></li>
        </ul>
        <ul class="social">
            <li>@if($contato->facebook)<a href="{{ $contato->facebook }}" class="facebook" target="_blank">facebook</a>@endif</li>
            <li>@if($contato->pinterest)<a href="{{ $contato->pinterest }}" class="pinterest" target="_blank">pinterest</a>@endif</li>
            <li>@if($contato->instagram)<a href="{{ $contato->instagram }}" class="instagram" target="_blank">instagram</a>@endif</li>
        </ul>
    </nav>
