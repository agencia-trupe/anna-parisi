@section('content')

    <div class="content help-mobile">
        <p>Clique para ver outras imagens</p>
    </div>

    <div class="content grid grid-mostras-mostras">
        <div class="gutter-sizer"></div>
        <div class="grid-sizer"></div>
        @foreach($mostras as $mostra)
        <a href="#" class="thumb thumb-masonry" data-galeria="{{ $mostra->id }}" title="{{ $mostra->titulo }}">
            <img src="{{ asset('assets/img/mostras/capa/'.$mostra->capa) }}" alt="">
            <div class="overlay">
                <span>{{ $mostra->titulo }}</span>
            </div>
        </a>
        @endforeach
    </div>

    {{-- <div class="hidden">
    @foreach($mostras as $mostra)
        @foreach($mostra->imagens as $imagem)
            <a href="{{ asset('assets/img/mostras/'.$imagem->imagem) }}" class="fancybox" rel="galeria{{ $mostra->id }}" title="{{ $mostra->titulo }}"></a>
        @endforeach
    @endforeach
    </div> --}}
    <script>
        var galerias = [];
        @foreach($mostras as $mostra)
            galerias[{{ $mostra->id }}] = [];
            @foreach($mostra->imagens as $imagem)
                galerias[{{ $mostra->id }}].push({
                    src: '{{ asset('assets/img/mostras/'.$imagem->imagem) }}',
                    title: '{{ $mostra->titulo }}',
                    w: {{ $imagem->largura }},
                    h: {{ $imagem->altura }},
                });
            @endforeach
        @endforeach
    </script>

@stop
