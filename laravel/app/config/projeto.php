<?php

return array(

    'name'        => 'Anna Parisi',
    'title'       => 'Anna Parisi . ARQUITETURA+DESIGN',
    'description' => 'Somos um estúdio de arquitetura fundado há 16 anos, sediado em São Paulo, que preza pela qualidade e personalismo de seus projetos. Anna Maria Parisi é a arquiteta pessoalmente à frente de todos os projetos do estúdio.',
    'descriptionfb' => 'Atuamos de forma integrada tanto em projetos residenciais e de incorporação, como em empreendimentos comerciais e corporativos.',
    'keywords'    => 'arquitetura, design de interiores, decoração de ambientes, projetos arquitetônicos, arquiteta, Anna Parisi, decoração, decoração de interiores',
    'share_image' => asset('assets/img/layout/annaparisi-fb2.jpg')

);
