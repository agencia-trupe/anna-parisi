-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2.2
-- http://www.phpmyadmin.net
--
-- Servidor: 179.188.16.21
-- Tempo de Geração: 21/10/2015 às 15:08:25
-- Versão do Servidor: 5.6.21
-- Versão do PHP: 5.3.3-7+squeeze27

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `trupe193`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 0, '20150602160658_slide1.jpg', '2015-06-02 19:06:59', '2015-06-02 19:06:59'),
(2, 0, '20150602160707_slide2.jpg', '2015-06-02 19:07:08', '2015-06-02 19:07:08'),
(3, 0, '20150602160715_slide3.jpg', '2015-06-02 19:07:16', '2015-06-02 19:07:16');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE IF NOT EXISTS `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `telefones` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `googlemaps` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pinterest` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `telefones`, `email`, `endereco`, `googlemaps`, `facebook`, `pinterest`, `instagram`, `created_at`, `updated_at`) VALUES
(1, '+55 11 98456 8470 | +55 11 2638 0782', 'contato@annaparisi.com.br', 'R. Jandiatuba 630 cj 528B - 05716-150 • São Paulo • SP', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.2018624777634!2d-46.7354086!3d-23.6329408!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5145ac6cc30b%3A0xe8484c79becef847!2sR.+Jandiatuba%2C+630+-+Vila+Andrade%2C+S%C3%A3o+Paulo+-+SP%2C+05716-150!5e0!3m2!1spt-BR!2sbr!4v1432834001344" width="600" height="450" frameborder="0" style="border:0"></iframe>', 'https://www.facebook.com/arquiteturaannamariaparisi', 'https://www.pinterest.com/amparisi/', 'https://instagram.com/annaparisi_arquitetura/', '0000-00-00 00:00:00', '2015-08-14 17:56:57');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--

CREATE TABLE IF NOT EXISTS `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `contatos_recebidos`
--

INSERT INTO `contatos_recebidos` (`id`, `nome`, `email`, `mensagem`, `lido`, `created_at`, `updated_at`) VALUES
(1, 'teste', 'teste@teste.com', '1 2 3', 'true', '2015-06-02 19:14:46', '2015-08-14 17:52:57');

-- --------------------------------------------------------

--
-- Estrutura da tabela `imprensa`
--

CREATE TABLE IF NOT EXISTS `imprensa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Extraindo dados da tabela `imprensa`
--

INSERT INTO `imprensa` (`id`, `data`, `titulo`, `capa`, `created_at`, `updated_at`) VALUES
(1, '2015-04', 'Decora Baby- Edção 80 -  Abril 2015', '20150807182712_Scan0001.jpg', '2015-06-02 19:11:37', '2015-08-10 16:58:29'),
(2, '2015-06', 'Decora Baby- Edção 82 - Junho 2015', '20150807182636_cap.jpg', '2015-06-02 19:11:46', '2015-08-10 16:58:14'),
(3, '2015-05', 'Casa & Construção - Ed. 116 - Maio 2015', '20150807201943_01.jpg', '2015-06-02 19:11:54', '2015-08-10 17:01:07'),
(4, '2011-02', 'Casa Claudia - Ed. 594 - Fevereiro 2011', '20150807204216_capa casaclaudia fev2011.jpg', '2015-06-02 19:11:58', '2015-08-10 16:52:33'),
(5, '2013-04', 'Casa Claudia - Ed. 620 - Abril 2013', '20150807205221_Scan0001.jpg', '2015-06-02 19:12:03', '2015-08-10 16:43:26'),
(6, '2011-07', 'Casa Claudia - Ed. 599 - Julho 2011', '20150807205457_capa.jpg', '2015-06-02 19:12:07', '2015-08-10 16:51:54'),
(7, '2012-07', 'Casa Claudia - Ed. 611 - Julho 2012', '20150807210529_capa.jpg', '2015-06-02 19:12:10', '2015-08-10 16:50:07'),
(8, '2014-01', 'Construir - Ed.176 ', '20150807210737_Scan0001.jpg', '2015-06-02 19:12:14', '2015-08-10 17:14:43'),
(9, '2014-09', 'Casa Claudia - Edição Especial Estantes ', '20150810153524_CAPA.jpg', '2015-08-10 18:35:24', '2015-08-10 18:35:43'),
(10, '2011-04', 'Casa Claudia - 150 Ideias de decoração - Volume III - Abril 2011 ', '20150810173611_FullSizeRender (3).jpg', '2015-08-10 20:36:12', '2015-08-10 20:36:12');

-- --------------------------------------------------------

--
-- Estrutura da tabela `imprensa_imagens`
--

CREATE TABLE IF NOT EXISTS `imprensa_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imprensa_id` int(11) NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;

--
-- Extraindo dados da tabela `imprensa_imagens`
--

INSERT INTO `imprensa_imagens` (`id`, `imprensa_id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(2, 2, 0, '20150807182131_01.JPG', '2015-08-07 21:21:32', '2015-08-07 21:21:32'),
(3, 2, 1, '20150807182143_02.JPG', '2015-08-07 21:21:44', '2015-08-07 21:21:44'),
(5, 2, 2, '20150807182218_03.JPG', '2015-08-07 21:22:18', '2015-08-07 21:22:18'),
(6, 1, 0, '20150807200747_Capturar 1.JPG', '2015-08-07 23:07:47', '2015-08-07 23:07:47'),
(7, 1, 1, '20150807200803_Capturar 2.JPG', '2015-08-07 23:08:04', '2015-08-07 23:08:04'),
(8, 1, 2, '20150807200825_Capturar 3.JPG', '2015-08-07 23:08:25', '2015-08-07 23:08:25'),
(9, 1, 3, '20150807200943_Capturar 4.JPG', '2015-08-07 23:09:43', '2015-08-07 23:09:43'),
(10, 3, 0, '20150807202519_Capturar 01.JPG', '2015-08-07 23:25:19', '2015-08-07 23:25:19'),
(11, 3, 1, '20150807202535_Capturar 02.JPG', '2015-08-07 23:25:35', '2015-08-07 23:25:35'),
(13, 4, 0, '20150807204832_CASACLAUDIA fev2011.jpg', '2015-08-07 23:48:33', '2015-08-07 23:48:33'),
(14, 5, 0, '20150807205307_Scan0002.jpg', '2015-08-07 23:53:08', '2015-08-07 23:53:08'),
(15, 6, 0, '20150807205550_Scan0001.jpg', '2015-08-07 23:55:51', '2015-08-07 23:55:51'),
(16, 7, 0, '20150807210501_Scan0001.jpg', '2015-08-08 00:05:02', '2015-08-08 00:05:02'),
(17, 8, 0, '20150807210758_Scan0002.jpg', '2015-08-08 00:07:59', '2015-08-08 00:07:59'),
(18, 8, 0, '20150807210812_Scan0003.jpg', '2015-08-08 00:08:13', '2015-08-08 00:08:13'),
(20, 9, 0, '20150810155442_FullSizeRender.jpg', '2015-08-10 18:54:42', '2015-08-10 18:54:42'),
(21, 9, 1, '20150810155807_03.jpg', '2015-08-10 18:58:08', '2015-08-10 18:58:08'),
(24, 10, 0, '20150810174913_FullSizeRender (4).jpg', '2015-08-10 20:49:13', '2015-08-10 20:49:13'),
(25, 10, 0, '20150810174925_FullSizeRender (5).jpg', '2015-08-10 20:49:25', '2015-08-10 20:49:25');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_01_28_130645_create_usuarios_table', 1),
('2015_05_28_163958_create_banners_table', 1),
('2015_05_28_164324_create_perfil_table', 1),
('2015_05_28_164438_create_projetos_table', 1),
('2015_05_28_164449_create_projetos_imagens_table', 1),
('2015_05_28_164505_create_mostras_table', 1),
('2015_05_28_164516_create_mostras_imagens_table', 1),
('2015_05_28_164536_create_imprensa_table', 1),
('2015_05_28_164547_create_imprensa_imagens_table', 1),
('2015_05_28_164918_create_contato_table', 1),
('2015_05_28_164929_create_contatos_recebidos_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `mostras`
--

CREATE TABLE IF NOT EXISTS `mostras` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Extraindo dados da tabela `mostras`
--

INSERT INTO `mostras` (`id`, `ordem`, `titulo`, `capa`, `created_at`, `updated_at`) VALUES
(7, 0, 'Em construção', '20151019174700_marca-AnnaParisi-square.jpg', '2015-10-19 19:47:00', '2015-10-19 19:47:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `mostras_imagens`
--

CREATE TABLE IF NOT EXISTS `mostras_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mostras_id` int(11) NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `mostras_imagens`
--

INSERT INTO `mostras_imagens` (`id`, `mostras_id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 1, 0, '20150602161712_annaparisi-8011.jpg', '2015-06-02 19:17:17', '2015-06-02 19:17:17'),
(2, 1, 0, '20150602161739_annaparisi-7997.jpg', '2015-06-02 19:17:46', '2015-06-02 19:17:46');

-- --------------------------------------------------------

--
-- Estrutura da tabela `perfil`
--

CREATE TABLE IF NOT EXISTS `perfil` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `perfil`
--

INSERT INTO `perfil` (`id`, `texto`, `created_at`, `updated_at`) VALUES
(1, '<p>Somos um est&uacute;dio de arquitetura fundado h&aacute; 16 anos, sediado&nbsp;em S&atilde;o Paulo, que preza pela qualidade e personalismo de seus projetos.</p>\r\n\r\n<p><strong>Anna Maria Parisi</strong> &eacute; a arquiteta pessoalmente &agrave; frente de todos os projetos do est&uacute;dio, com forma&ccedil;&atilde;o no Brasil e na It&aacute;lia.&nbsp;Nossa principal caracter&iacute;stica &eacute; a determina&ccedil;&atilde;o em participar pessoalmente de cada projeto de cada cliente, com total aten&ccedil;&atilde;o aos detalhes e peculiaridades de cada situa&ccedil;&atilde;o e sua personaliza&ccedil;&atilde;o. N&atilde;o delegamos a assistentes e fornecedores a ess&ecirc;ncia do nosso trabalho, e colocamos a paix&atilde;o e o entusiasmo que ir&atilde;o fazer a diferen&ccedil;a no resultado final.</p>\r\n\r\n<p>Por isso mesmo, nosso trabalho engloba n&atilde;o somente os projetos de arquitetura, decora&ccedil;&atilde;o e design, mas tamb&eacute;m todo o gerenciamento da obra e do projeto de modo a tornar realidade o sonho idealizado no in&iacute;cio. Temos como miss&atilde;o superar as expectativas do cliente e proporcionar uma &oacute;tima experi&ecirc;ncia.</p>\r\n\r\n<p>Atuamos de forma integrada tanto em projetos residenciais e de incorpora&ccedil;&atilde;o, como em empreendimentos comerciais e corporativos.&nbsp;</p>\r\n', '0000-00-00 00:00:00', '2015-10-21 17:00:49');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projetos`
--

CREATE TABLE IF NOT EXISTS `projetos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Extraindo dados da tabela `projetos`
--

INSERT INTO `projetos` (`id`, `ordem`, `titulo`, `capa`, `created_at`, `updated_at`) VALUES
(1, 0, 'Apartamento Planalto Paulista - São Paulo', '20150814134604_annaparisi-8003.jpg', '2015-06-02 19:08:34', '2015-08-14 16:46:35'),
(2, 4, 'Apartamento Morumbi III  - São Paulo', '20151020201415__FJR3923 (2).JPG', '2015-06-02 19:08:39', '2015-10-20 22:14:19'),
(3, 7, 'Apartamento Vila Madalena - São Paulo', '20150812185241_JJ 025.jpg', '2015-06-02 19:08:43', '2015-10-21 16:17:30'),
(5, 5, 'Apartamento Morumbi II - São Paulo', '20150812211046__FJR3128.JPG', '2015-06-02 19:08:51', '2015-10-20 20:33:50'),
(6, 9, 'Apartamento Morumbi I - São Paulo', '20151021133111_IMG_6477 - Cópia.JPG', '2015-08-13 23:19:33', '2015-10-21 15:31:13'),
(7, 8, 'Ateliê de artista plástico - Morumbi - São Paulo', '20150911191757_21.jpg', '2015-09-11 22:13:08', '2015-10-21 16:17:43'),
(8, 10, 'Hall de entrada  do  Ed. Gran Palazzo Ducale - Vila Mariana - São Paulo ', '20150911195621_tt 046.jpg', '2015-09-11 22:56:24', '2015-10-21 16:19:50'),
(9, 11, 'Residência Alto de Pinheiros - São Paulo', '20150914143727_Fachada interna depois.jpg', '2015-09-14 17:37:28', '2015-10-19 19:36:15'),
(10, 3, 'Quarto para gêmeos - São Paulo', '20151020193048_IMG_0643.jpg', '2015-09-14 18:03:42', '2015-10-20 21:30:51'),
(11, 6, 'Quarto Meninas e Brinquedoteca - São Paulo', '20150915181940_IMG_7467_Julia_Ribeiro.jpg', '2015-09-15 21:19:46', '2015-10-19 19:39:20'),
(12, 1, 'Dormitório menina - São Paulo ', '20151020195154_IMG_0096.jpg', '2015-10-19 15:01:13', '2015-10-21 16:18:50'),
(13, 2, 'Dormitório Bebê  - São Paulo', '20151019150606_Panorama4.jpg', '2015-10-19 17:06:09', '2015-10-21 16:19:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projetos_imagens`
--

CREATE TABLE IF NOT EXISTS `projetos_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `projetos_id` int(11) NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=155 ;

--
-- Extraindo dados da tabela `projetos_imagens`
--

INSERT INTO `projetos_imagens` (`id`, `projetos_id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '20150602161537_annaparisi-7995.jpg', '2015-06-02 19:15:43', '2015-06-02 19:15:43'),
(7, 1, 10, '20150803195919_annaparisi-8054.jpg', '2015-08-03 22:59:23', '2015-08-03 22:59:23'),
(8, 1, 11, '20150804135112_annaparisi-8060.jpg', '2015-08-04 16:51:50', '2015-08-04 16:51:50'),
(9, 1, 8, '20150804135402_annaparisi-8044.jpg', '2015-08-04 16:54:13', '2015-08-04 16:54:13'),
(10, 1, 9, '20150804135606_annaparisi-8045.jpg', '2015-08-04 16:56:22', '2015-08-04 16:56:22'),
(11, 1, 12, '20150804135924_annaparisi-8059.jpg', '2015-08-04 16:59:31', '2015-08-04 16:59:31'),
(12, 1, 13, '20150804140218_annaparisi-8065.jpg', '2015-08-04 17:02:25', '2015-08-04 17:02:25'),
(14, 1, 3, '20150804140711_annaparisi-8016.jpg', '2015-08-04 17:07:42', '2015-08-04 17:07:42'),
(15, 1, 7, '20150804141323_annaparisi-7990.jpg', '2015-08-04 17:13:48', '2015-08-04 17:13:48'),
(17, 2, 6, '20150804144206__FJR3121.JPG', '2015-08-04 17:42:25', '2015-08-04 17:42:25'),
(18, 2, 2, '20150804144813__FJR3101.JPG', '2015-08-04 17:48:48', '2015-08-04 17:48:48'),
(19, 1, 0, '20150804145323_annaparisi-8003.jpg', '2015-08-04 17:53:43', '2015-08-04 17:53:43'),
(30, 3, 9, '20150807145426_amparisi 016.jpg', '2015-08-07 17:54:28', '2015-08-07 17:54:28'),
(33, 3, 10, '20150807150717_amparisi 061 - Cópia.jpg', '2015-08-07 18:07:19', '2015-08-07 18:07:19'),
(36, 3, 7, '20150810202713_JJ 001.jpg', '2015-08-10 23:27:15', '2015-08-10 23:27:15'),
(37, 3, 8, '20150811134410_JJ 004.jpg', '2015-08-11 16:44:16', '2015-08-11 16:44:16'),
(38, 3, 6, '20150811134449_JJ 006.jpg', '2015-08-11 16:44:53', '2015-08-11 16:44:53'),
(40, 3, 0, '20150811135255_JJ 025.jpg', '2015-08-11 16:52:58', '2015-08-11 16:52:58'),
(42, 3, 11, '20150812173413_JJ 142.jpg', '2015-08-12 20:34:16', '2015-08-12 20:34:16'),
(43, 3, 5, '20150812174450_JJ 149.jpg', '2015-08-12 20:44:54', '2015-08-12 20:44:54'),
(44, 3, 2, '20150812174539_JJ 155.jpg', '2015-08-12 20:45:42', '2015-08-12 20:45:42'),
(45, 3, 4, '20150812174629_JJ 015.jpg', '2015-08-12 20:46:31', '2015-08-12 20:46:31'),
(46, 3, 3, '20150812174748_JJ 020.jpg', '2015-08-12 20:47:51', '2015-08-12 20:47:51'),
(47, 3, 1, '20150812185120_JJ 157.jpg', '2015-08-12 21:51:23', '2015-08-12 21:51:23'),
(48, 5, 9, '20150812185628__FJR3128.JPG', '2015-08-12 21:56:39', '2015-08-12 21:56:39'),
(49, 5, 1, '20150812210613__FJR3144.JPG', '2015-08-13 00:06:24', '2015-08-13 00:06:24'),
(50, 5, 2, '20150813132520__FJR3136.JPG', '2015-08-13 16:25:28', '2015-08-13 16:25:28'),
(52, 5, 4, '20150813135749__FJR3144.JPG', '2015-08-13 16:58:06', '2015-08-13 16:58:06'),
(55, 5, 6, '20150813145645__FJR3152.JPG', '2015-08-13 17:56:55', '2015-08-13 17:56:55'),
(57, 5, 8, '20150813174935__FJR3158.JPG', '2015-08-13 20:49:43', '2015-08-13 20:49:43'),
(58, 5, 5, '20150813195828__FJR3163.JPG', '2015-08-13 22:58:37', '2015-08-13 22:58:37'),
(59, 6, 0, '20150813202259_IMG_6479.JPG', '2015-08-13 23:23:01', '2015-08-13 23:23:01'),
(60, 6, 2, '20150813202521_3291copia.jpg', '2015-08-13 23:25:23', '2015-08-13 23:25:23'),
(61, 6, 1, '20150813202653_IMG_6470.JPG', '2015-08-13 23:26:55', '2015-08-13 23:26:55'),
(62, 6, 3, '20150813202817_3290.JPG', '2015-08-13 23:28:19', '2015-08-13 23:28:19'),
(63, 6, 4, '20150813203000_3289.JPG', '2015-08-13 23:30:02', '2015-08-13 23:30:02'),
(69, 2, 7, '20150814141249__FJR3125.JPG', '2015-08-14 17:12:59', '2015-08-14 17:12:59'),
(72, 2, 3, '20150814143821__FJR3103 2.jpg', '2015-08-14 17:38:27', '2015-08-14 17:38:27'),
(73, 2, 1, '20150819201553__FJR3917.JPG', '2015-08-19 23:15:59', '2015-08-19 23:15:59'),
(76, 2, 0, '20150819213028__FJR3923.JPG', '2015-08-20 00:30:32', '2015-08-20 00:30:32'),
(77, 7, 0, '20150911191338_AP2.JPG', '2015-09-11 22:13:38', '2015-09-11 22:13:38'),
(79, 7, 8, '20150911191609_AP3.JPG', '2015-09-11 22:16:09', '2015-09-11 22:16:09'),
(80, 7, 1, '20150911193337_AP4.JPG', '2015-09-11 22:33:38', '2015-09-11 22:33:38'),
(82, 7, 9, '20150911193422_AP6.JPG', '2015-09-11 22:34:23', '2015-09-11 22:34:23'),
(83, 7, 7, '20150911194112_AP7.JPG', '2015-09-11 22:41:13', '2015-09-11 22:41:13'),
(85, 7, 5, '20150911194143_AP8.JPG', '2015-09-11 22:41:44', '2015-09-11 22:41:44'),
(86, 7, 4, '20150911194206_AP10.JPG', '2015-09-11 22:42:08', '2015-09-11 22:42:08'),
(87, 7, 3, '20150911194309_AP11.JPG', '2015-09-11 22:43:10', '2015-09-11 22:43:10'),
(88, 7, 2, '20150911194334_AP12.JPG', '2015-09-11 22:43:35', '2015-09-11 22:43:35'),
(93, 8, 0, '20150914141911_AP18.JPG', '2015-09-14 17:19:11', '2015-09-14 17:19:11'),
(94, 8, 0, '20150914142003_AP19.JPG', '2015-09-14 17:20:03', '2015-09-14 17:20:03'),
(95, 8, 0, '20150914142015_AP20.JPG', '2015-09-14 17:20:15', '2015-09-14 17:20:15'),
(96, 8, 0, '20150914142029_AP21.JPG', '2015-09-14 17:20:29', '2015-09-14 17:20:29'),
(100, 9, 0, '20150914144611_AP14.JPG', '2015-09-14 17:46:11', '2015-09-14 17:46:11'),
(101, 9, 2, '20150914144636_AP17.JPG', '2015-09-14 17:46:37', '2015-09-14 17:46:37'),
(102, 9, 1, '20150914144834_AP16.JPG', '2015-09-14 17:48:35', '2015-09-14 17:48:35'),
(106, 10, 4, '20150915152101_annaparisi-8035.jpg', '2015-09-15 18:21:09', '2015-09-15 18:21:09'),
(107, 11, 0, '20150916141002_IMG_7467_Julia_Ribeiro.jpg', '2015-09-16 17:10:11', '2015-09-16 17:10:11'),
(108, 11, 1, '20150916141337_IMG_7468_Julia_Ribeiro.jpg', '2015-09-16 17:13:47', '2015-09-16 17:13:47'),
(109, 11, 2, '20150916153202_IMG_7478_Julia_Ribeiro.jpg', '2015-09-16 18:32:08', '2015-09-16 18:32:08'),
(110, 1, 1, '20150916153650_annaparisi-8011.jpg', '2015-09-16 18:36:58', '2015-09-16 18:36:58'),
(111, 1, 6, '20150916154815_annaparisi-8010.jpg', '2015-09-16 18:48:23', '2015-09-16 18:48:23'),
(112, 1, 4, '20150916155959_annaparisi-8031.jpg', '2015-09-16 19:00:07', '2015-09-16 19:00:07'),
(113, 5, 7, '20150921195650__FJR3134.JPG', '2015-09-21 22:56:56', '2015-09-21 22:56:56'),
(114, 5, 0, '20150921200529__FJR3146.JPG', '2015-09-21 23:05:36', '2015-09-21 23:05:36'),
(116, 12, 0, '20151019130646_IMG_0096.jpg', '2015-10-19 15:06:51', '2015-10-19 15:06:51'),
(117, 12, 7, '20151019131632_IMG_0125.jpg', '2015-10-19 15:16:35', '2015-10-19 15:16:35'),
(118, 12, 2, '20151019134049_IMG_0163.jpg', '2015-10-19 15:40:53', '2015-10-19 15:40:53'),
(119, 12, 4, '20151019135823_IMG_0114.jpg', '2015-10-19 15:58:27', '2015-10-19 15:58:27'),
(120, 12, 1, '20151019144100_IMG_0090.jpg', '2015-10-19 16:41:04', '2015-10-19 16:41:04'),
(121, 12, 5, '20151019150223_IMG_0138.jpg', '2015-10-19 17:02:28', '2015-10-19 17:02:28'),
(122, 13, 0, '20151019150737_Panorama4.jpg', '2015-10-19 17:07:42', '2015-10-19 17:07:42'),
(123, 13, 3, '20151019161539_IMG_0038.jpg', '2015-10-19 18:15:44', '2015-10-19 18:15:44'),
(124, 13, 6, '20151019161724_IMG_0031.jpg', '2015-10-19 18:17:32', '2015-10-19 18:17:32'),
(125, 13, 1, '20151019161930_IMG_9989.jpg', '2015-10-19 18:19:34', '2015-10-19 18:19:34'),
(126, 13, 4, '20151019162433_IMG_9961.jpg', '2015-10-19 18:24:38', '2015-10-19 18:24:38'),
(127, 13, 5, '20151019165348_IMG_9994.jpg', '2015-10-19 18:53:51', '2015-10-19 18:53:51'),
(128, 13, 2, '20151019165522_IMG_9992.jpg', '2015-10-19 18:55:26', '2015-10-19 18:55:26'),
(129, 13, 7, '20151019165728_IMG_0065.jpg', '2015-10-19 18:57:32', '2015-10-19 18:57:32'),
(131, 12, 3, '20151019171428_IMG_0072.jpg', '2015-10-19 19:14:33', '2015-10-19 19:14:33'),
(132, 12, 6, '20151019171732_IMG_0119.jpg', '2015-10-19 19:17:41', '2015-10-19 19:17:41'),
(133, 10, 0, '20151019184637_IMG_0643.jpg', '2015-10-19 20:46:38', '2015-10-19 20:46:38'),
(134, 10, 1, '20151019185947_IMG_0640.jpg', '2015-10-19 20:59:48', '2015-10-19 20:59:48'),
(135, 10, 3, '20151019191317_IMG_0659.jpg', '2015-10-19 21:13:19', '2015-10-19 21:13:19'),
(136, 10, 2, '20151019191619_IMG_0663.jpg', '2015-10-19 21:16:20', '2015-10-19 21:16:20'),
(137, 1, 5, '20151020135326_annaparisi-8008.jpg', '2015-10-20 15:53:41', '2015-10-20 15:53:41'),
(138, 11, 3, '20151020164827_IMG_7463_Julia_Ribeiro.jpg', '2015-10-20 18:49:01', '2015-10-20 18:49:01'),
(139, 11, 4, '20151020165304_IMG_7497_Julia_Ribeiro.jpg', '2015-10-20 18:53:19', '2015-10-20 18:53:19'),
(140, 11, 6, '20151020170901_IMG_7447_Julia_Ribeiro.jpg', '2015-10-20 19:09:20', '2015-10-20 19:09:20'),
(141, 11, 5, '20151020171242_IMG_7453_Julia_Ribeiro.jpg', '2015-10-20 19:12:55', '2015-10-20 19:12:55'),
(142, 11, 12, '20151020171532_IMG_7528_Julia_Ribeiro.jpg', '2015-10-20 19:15:59', '2015-10-20 19:15:59'),
(143, 11, 7, '20151020171817_IMG_7520_Julia_Ribeiro.jpg', '2015-10-20 19:18:36', '2015-10-20 19:18:36'),
(144, 11, 10, '20151020172215_IMG_7525_Julia_Ribeiro.jpg', '2015-10-20 19:22:54', '2015-10-20 19:22:54'),
(145, 11, 8, '20151020172522_IMG_7518_Julia_Ribeiro.jpg', '2015-10-20 19:25:46', '2015-10-20 19:25:46'),
(146, 11, 13, '20151020172918_IMG_7512_Julia_Ribeiro.jpg', '2015-10-20 19:29:41', '2015-10-20 19:29:41'),
(147, 11, 11, '20151020173311_IMG_7527_Julia_Ribeiro.jpg', '2015-10-20 19:34:00', '2015-10-20 19:34:00'),
(148, 10, 5, '20151020185052_annaparisi-8041.jpg', '2015-10-20 20:51:30', '2015-10-20 20:51:30'),
(150, 10, 6, '20151020191302_annaparisi-8042 (2).jpg', '2015-10-20 21:13:13', '2015-10-20 21:13:13'),
(151, 11, 9, '20151020195810_IMG_7538_Julia_Ribeiro.jpg', '2015-10-20 21:58:30', '2015-10-20 21:58:30'),
(152, 10, 7, '20151021132644_IMG_0670.jpg', '2015-10-21 15:26:45', '2015-10-21 15:26:45'),
(153, 2, 4, '20151021134126__FJR3934 (2).JPG', '2015-10-21 15:41:35', '2015-10-21 15:41:35'),
(154, 2, 5, '20151021134524__FJR3935.JPG', '2015-10-21 15:45:34', '2015-10-21 15:45:34');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuarios_email_unique` (`email`),
  UNIQUE KEY `usuarios_username_unique` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `email`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'contato@trupe.net', 'trupe', '$2y$10$9/E/llTAPQw.rzGw2JvWp./lxB.QAaRJSc8OprwuiERrx/DgPeAhO', 'xcF95Ah8cwEcfqbXCAqcSp8lz6hnKFtQKd97EZyo1fTUfE8CamETVBd6VTAD', '0000-00-00 00:00:00', '2015-06-08 17:03:28'),
(2, 'contato@annaparisi.com.br', 'anna', '$2y$10$j1M0jmIop4tT40DzSFp8iuN7q3Wp73HAZYZ4W6CQXnlpw.48CnD4G', 'w7To3GB76wAnRWoQCzXbvTsq46hxuyihJVf988oh7H9JugsAAKTRI0JwDYsU', '2015-06-08 17:03:23', '2015-06-09 21:41:56');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
