-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: 179.188.16.80
-- Generation Time: 27-Jan-2016 às 12:20
-- Versão do servidor: 5.6.21-69.0-log
-- PHP Version: 5.6.14-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `annamariaparisi1`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 9, '20150602160658_slide1.jpg', '2015-06-02 19:06:59', '2015-06-02 19:06:59'),
(4, 0, '20151027175019_01_Estar IMG_0827-1.jpg', '2015-10-27 19:50:25', '2015-10-27 19:50:25'),
(5, 1, '20151027175846_01_Jantar IMG_0776-1.jpg', '2015-10-27 19:58:52', '2015-10-27 19:58:52'),
(6, 2, '20151027180438_annaparisi-8003.jpg', '2015-10-27 20:04:46', '2015-10-27 20:04:46'),
(7, 5, '20151027183332_01_Estar IMG_0794-1.jpg', '2015-10-27 20:33:38', '2015-10-27 20:33:38'),
(8, 6, '20151028172602__FJR3932.JPG', '2015-10-28 19:26:07', '2015-10-28 19:26:07'),
(9, 7, '20151028172923__FJR3923.JPG', '2015-10-28 19:29:26', '2015-10-28 19:29:26'),
(10, 4, '20151028173110_01_Estar IMG_0818-1.jpg', '2015-10-28 19:31:15', '2015-10-28 19:31:15'),
(12, 3, '20151028175303_annaparisi-8059.jpg', '2015-10-28 19:53:10', '2015-10-28 19:53:10'),
(14, 10, '20151028180008_Untitled_Panorama1-2-2.jpg', '2015-10-28 20:00:10', '2015-10-28 20:00:10'),
(16, 0, '20151029174634_01_Estar IMG_0820-1.jpg', '2015-10-29 19:46:39', '2015-10-29 19:46:39');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(10) UNSIGNED NOT NULL,
  `telefones` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `googlemaps` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pinterest` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `telefones`, `email`, `endereco`, `googlemaps`, `facebook`, `pinterest`, `instagram`, `created_at`, `updated_at`) VALUES
(1, '+55 11 98456 8470 | +55 11 2638 0782', 'contato@annaparisi.com.br', 'R. Jandiatuba 630 cj 528B - 05716-150 • São Paulo • SP', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.2018624777634!2d-46.7354086!3d-23.6329408!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5145ac6cc30b%3A0xe8484c79becef847!2sR.+Jandiatuba%2C+630+-+Vila+Andrade%2C+S%C3%A3o+Paulo+-+SP%2C+05716-150!5e0!3m2!1spt-BR!2sbr!4v1432834001344" width="600" height="450" frameborder="0" style="border:0"></iframe>', 'https://www.facebook.com/arquiteturaannamariaparisi', 'https://www.pinterest.com/amparisi/', 'https://instagram.com/annaparisi_arquitetura/', '0000-00-00 00:00:00', '2015-08-14 17:56:57');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--

CREATE TABLE `contatos_recebidos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contatos_recebidos`
--

INSERT INTO `contatos_recebidos` (`id`, `nome`, `email`, `mensagem`, `lido`, `created_at`, `updated_at`) VALUES
(2, 'Leila', 'leila@trupe.net', 'teste de envio de email a partir do formulário do site.', 'true', '2015-10-23 22:30:29', '2015-10-27 20:35:06'),
(3, 'THAISA BLANSKI SCHNEKENBERG', 'tblanski@gmail.com', 'Teste ', 'false', '2015-10-29 14:20:05', '2015-10-29 14:20:05'),
(4, 'Paulo Márcio', 'pmvdarocha@uol.com.br', 'Parabéns, Anna, pelo novo site.\nProfissionalismo, seriedade e competência, sem prejuízo da sensibilidade para perceber o outro, única forma de viabilizar sonhos alheios.\nAdmiro muito o seu trabalho.', 'false', '2015-12-15 02:57:16', '2015-12-15 02:57:16');

-- --------------------------------------------------------

--
-- Estrutura da tabela `imprensa`
--

CREATE TABLE `imprensa` (
  `id` int(10) UNSIGNED NOT NULL,
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `imprensa`
--

INSERT INTO `imprensa` (`id`, `data`, `titulo`, `capa`, `created_at`, `updated_at`) VALUES
(1, '2015-04', 'Decora Baby- Edção 80 -  Abril 2015', '20150807182712_Scan0001.jpg', '2015-06-02 19:11:37', '2015-08-10 16:58:29'),
(2, '2015-06', 'Decora Baby- Edção 82 - Junho 2015', '20150807182636_cap.jpg', '2015-06-02 19:11:46', '2015-08-10 16:58:14'),
(3, '2015-05', 'Casa & Construção - Ed. 116 - Maio 2015', '20150807201943_01.jpg', '2015-06-02 19:11:54', '2015-08-10 17:01:07'),
(4, '2011-02', 'Casa Claudia - Ed. 594 - Fevereiro 2011', '20150807204216_capa casaclaudia fev2011.jpg', '2015-06-02 19:11:58', '2015-08-10 16:52:33'),
(5, '2013-04', 'Casa Claudia - Ed. 620 - Abril 2013', '20150807205221_Scan0001.jpg', '2015-06-02 19:12:03', '2015-08-10 16:43:26'),
(6, '2011-07', 'Casa Claudia - Ed. 599 - Julho 2011', '20150807205457_capa.jpg', '2015-06-02 19:12:07', '2015-08-10 16:51:54'),
(7, '2012-07', 'Casa Claudia - Ed. 611 - Julho 2012', '20150807210529_capa.jpg', '2015-06-02 19:12:10', '2015-08-10 16:50:07'),
(8, '2014-01', 'Construir - Ed.176 ', '20150807210737_Scan0001.jpg', '2015-06-02 19:12:14', '2015-08-10 17:14:43'),
(9, '2014-09', 'Casa Claudia - Edição Especial Estantes ', '20150810153524_CAPA.jpg', '2015-08-10 18:35:24', '2015-08-10 18:35:43'),
(10, '2011-04', 'Casa Claudia - 150 Ideias de decoração - Volume III - Abril 2011 ', '20150810173611_FullSizeRender (3).jpg', '2015-08-10 20:36:12', '2015-08-10 20:36:12');

-- --------------------------------------------------------

--
-- Estrutura da tabela `imprensa_imagens`
--

CREATE TABLE `imprensa_imagens` (
  `id` int(10) UNSIGNED NOT NULL,
  `imprensa_id` int(11) NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `largura` int(11) DEFAULT NULL,
  `altura` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `imprensa_imagens`
--

INSERT INTO `imprensa_imagens` (`id`, `imprensa_id`, `ordem`, `imagem`, `largura`, `altura`, `created_at`, `updated_at`) VALUES
(2, 2, 0, '20150807182131_01.JPG', 1371, 920, '2015-08-07 21:21:32', '2016-01-27 16:01:57'),
(3, 2, 1, '20150807182143_02.JPG', 1427, 957, '2015-08-07 21:21:44', '2016-01-27 16:01:57'),
(5, 2, 2, '20150807182218_03.JPG', 1425, 957, '2015-08-07 21:22:18', '2016-01-27 16:01:57'),
(6, 1, 0, '20150807200747_Capturar 1.JPG', 683, 921, '2015-08-07 23:07:47', '2016-01-27 16:01:58'),
(7, 1, 1, '20150807200803_Capturar 2.JPG', 685, 920, '2015-08-07 23:08:04', '2016-01-27 16:01:58'),
(8, 1, 2, '20150807200825_Capturar 3.JPG', 684, 918, '2015-08-07 23:08:25', '2016-01-27 16:01:58'),
(9, 1, 3, '20150807200943_Capturar 4.JPG', 682, 918, '2015-08-07 23:09:43', '2016-01-27 16:01:58'),
(10, 3, 0, '20150807202519_Capturar 01.JPG', 688, 917, '2015-08-07 23:25:19', '2016-01-27 16:01:58'),
(11, 3, 1, '20150807202535_Capturar 02.JPG', 685, 920, '2015-08-07 23:25:35', '2016-01-27 16:01:58'),
(13, 4, 0, '20150807204832_CASACLAUDIA fev2011.jpg', 1091, 1606, '2015-08-07 23:48:33', '2016-01-27 16:01:58'),
(14, 5, 0, '20150807205307_Scan0002.jpg', 1599, 2103, '2015-08-07 23:53:08', '2016-01-27 16:01:58'),
(15, 6, 0, '20150807205550_Scan0001.jpg', 1471, 2125, '2015-08-07 23:55:51', '2016-01-27 16:01:58'),
(16, 7, 0, '20150807210501_Scan0001.jpg', 1471, 2133, '2015-08-08 00:05:02', '2016-01-27 16:01:58'),
(17, 8, 0, '20150807210758_Scan0002.jpg', 1596, 2082, '2015-08-08 00:07:59', '2016-01-27 16:01:58'),
(18, 8, 0, '20150807210812_Scan0003.jpg', 1575, 2129, '2015-08-08 00:08:13', '2016-01-27 16:01:58'),
(20, 9, 0, '20150810155442_FullSizeRender.jpg', 1276, 878, '2015-08-10 18:54:42', '2016-01-27 16:01:58'),
(21, 9, 1, '20150810155807_03.jpg', 902, 1266, '2015-08-10 18:58:08', '2016-01-27 16:01:58'),
(24, 10, 0, '20150810174913_FullSizeRender (4).jpg', 1468, 970, '2015-08-10 20:49:13', '2016-01-27 16:01:58'),
(25, 10, 0, '20150810174925_FullSizeRender (5).jpg', 1264, 807, '2015-08-10 20:49:25', '2016-01-27 16:01:58');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_01_28_130645_create_usuarios_table', 1),
('2015_05_28_163958_create_banners_table', 1),
('2015_05_28_164324_create_perfil_table', 1),
('2015_05_28_164438_create_projetos_table', 1),
('2015_05_28_164449_create_projetos_imagens_table', 1),
('2015_05_28_164505_create_mostras_table', 1),
('2015_05_28_164516_create_mostras_imagens_table', 1),
('2015_05_28_164536_create_imprensa_table', 1),
('2015_05_28_164547_create_imprensa_imagens_table', 1),
('2015_05_28_164918_create_contato_table', 1),
('2015_05_28_164929_create_contatos_recebidos_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `mostras`
--

CREATE TABLE `mostras` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `mostras`
--

INSERT INTO `mostras` (`id`, `ordem`, `titulo`, `capa`, `created_at`, `updated_at`) VALUES
(7, 0, 'Em construção', '20151019174700_marca-AnnaParisi-square.jpg', '2015-10-19 19:47:00', '2015-10-19 19:47:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `mostras_imagens`
--

CREATE TABLE `mostras_imagens` (
  `id` int(10) UNSIGNED NOT NULL,
  `mostras_id` int(11) NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `largura` int(11) DEFAULT NULL,
  `altura` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `mostras_imagens`
--

INSERT INTO `mostras_imagens` (`id`, `mostras_id`, `ordem`, `imagem`, `largura`, `altura`, `created_at`, `updated_at`) VALUES
(1, 1, 0, '20150602161712_annaparisi-8011.jpg', 3000, 2127, '2015-06-02 19:17:17', '2016-01-27 16:01:28'),
(2, 1, 0, '20150602161739_annaparisi-7997.jpg', 3000, 1947, '2015-06-02 19:17:46', '2016-01-27 16:01:29');

-- --------------------------------------------------------

--
-- Estrutura da tabela `perfil`
--

CREATE TABLE `perfil` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `perfil`
--

INSERT INTO `perfil` (`id`, `texto`, `created_at`, `updated_at`) VALUES
(1, '<p>Somos um est&uacute;dio de arquitetura fundado h&aacute; 16 anos, sediado&nbsp;em S&atilde;o Paulo, que preza pela qualidade e personalismo de seus projetos.</p>\r\n\r\n<p><strong>Anna Maria Parisi</strong> &eacute; a arquiteta pessoalmente &agrave; frente de todos os projetos do est&uacute;dio, com forma&ccedil;&atilde;o no Brasil e na It&aacute;lia.&nbsp;Nossa principal caracter&iacute;stica &eacute; a determina&ccedil;&atilde;o em participar pessoalmente de cada projeto de cada cliente, com total aten&ccedil;&atilde;o aos detalhes e peculiaridades de cada situa&ccedil;&atilde;o e sua personaliza&ccedil;&atilde;o. N&atilde;o delegamos a assistentes e fornecedores a ess&ecirc;ncia do nosso trabalho, e colocamos a paix&atilde;o e o entusiasmo que ir&atilde;o fazer a diferen&ccedil;a no resultado final.</p>\r\n\r\n<p>Por isso mesmo, nosso trabalho engloba n&atilde;o somente os projetos de arquitetura, decora&ccedil;&atilde;o e design, mas tamb&eacute;m todo o gerenciamento da obra e do projeto de modo a tornar realidade o sonho idealizado no in&iacute;cio. Temos como miss&atilde;o superar as expectativas do cliente e proporcionar uma &oacute;tima experi&ecirc;ncia.</p>\r\n\r\n<p>Atuamos de forma integrada tanto em projetos residenciais e de incorpora&ccedil;&atilde;o, como em empreendimentos comerciais e corporativos.&nbsp;</p>\r\n', '0000-00-00 00:00:00', '2015-10-21 17:00:49');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projetos`
--

CREATE TABLE `projetos` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `projetos`
--

INSERT INTO `projetos` (`id`, `ordem`, `titulo`, `capa`, `created_at`, `updated_at`) VALUES
(1, 1, 'Apartamento Planalto Paulista - São Paulo', '20151023141207_annaparisi-8003 (2).jpg', '2015-06-02 19:08:34', '2015-10-23 16:12:10'),
(2, 2, 'Apartamento Morumbi III  - São Paulo', '20151022191203__FJR3935 (2).JPG', '2015-06-02 19:08:39', '2015-10-22 21:12:11'),
(3, 14, 'Apartamento Vila Madalena - São Paulo', '20150812185241_JJ 025.jpg', '2015-06-02 19:08:43', '2015-10-21 16:17:30'),
(5, 13, 'Apartamento Morumbi II - São Paulo', '20151022193559__FJR3146 - Cópia.JPG', '2015-06-02 19:08:51', '2015-10-22 21:36:04'),
(6, 17, 'Apartamento Morumbi I - São Paulo', '20151022192419_IMG_6477 - Cópia - Cópia.JPG', '2015-08-13 23:19:33', '2015-10-22 21:24:20'),
(7, 18, 'Ateliê de artista plástico - Morumbi - São Paulo', '20150911191757_21.jpg', '2015-09-11 22:13:08', '2015-10-21 16:17:43'),
(8, 24, 'Hall de entrada  do  Ed. Gran Palazzo Ducale - Vila Mariana - São Paulo ', '20151022192057_tt 034 - Cópia.jpg', '2015-09-11 22:56:24', '2015-10-22 21:21:00'),
(10, 10, 'Quarto Gêmeos - São Paulo', '20151023141248_IMG_0640.jpg', '2015-09-14 18:03:42', '2015-10-27 21:29:49'),
(11, 16, 'Quarto Meninas  - São Paulo', '20151022184700_IMG_7473_Julia_Ribeiro.jpg', '2015-09-15 21:19:46', '2015-10-22 20:49:08'),
(12, 11, 'Dormitório Menina - Campo Belo SP', '20151028164539_Untitled_Panorama1-2-2.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 12, 'Dormitório Bebê  - São Paulo', '20151022141713_IMG_0017.jpg', '2015-10-19 17:06:09', '2015-10-22 16:17:16'),
(14, 22, 'Apartamento Planalto Paulista - São Paulo', '20151022124843_annaparisi-8065 (2).jpg', '2015-10-22 14:48:50', '2015-10-23 21:10:03'),
(15, 4, 'Apartamento Planalto Paulista - São Paulo', '20151022130159_annaparisi-7990 (2).jpg', '2015-10-22 15:02:03', '2015-10-28 19:31:59'),
(16, 8, 'Dormitório Casal - Planalto paulista SP', '20151022133549_annaparisi-8044.jpg', '2015-10-22 15:35:59', '2015-10-27 21:26:27'),
(17, 6, 'Apartamento Planalto Paulista - São Paulo', '20151022135129_annaparisi-8060.jpg', '2015-10-22 15:51:48', '2015-10-23 21:05:01'),
(18, 20, 'Brinquedoteca - São Paulo ', '20151022175330_IMG_7537_Julia_Ribeiro.jpg', '2015-10-22 19:06:17', '2015-10-22 19:53:39'),
(19, 15, 'Dormitório  Casal - Vila. Madalena SP', '20151023132535_amparisi 061 - Cópia.jpg', '2015-10-23 15:25:41', '2015-10-27 21:28:46'),
(20, 23, 'Dormitório Adolescente - São Paulo ', '20151023184948_amparisi 059 - Cópia.jpg', '2015-10-23 15:44:50', '2015-10-27 21:30:41'),
(21, 21, 'Apartamento Morumbi II - São Paulo', '20151023143757__FJR3158.JPG', '2015-10-23 16:38:04', '2015-10-23 16:38:04'),
(22, 19, 'Apartamento Morumbi II - São Paulo', '20151023171135__FJR3144 (2).JPG', '2015-10-23 19:11:42', '2015-10-23 19:11:42'),
(23, 0, 'Apartamento Perdizes - São Paulo', '20151027185231_01_Estar IMG_0794-1.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 3, 'Dormitório Casal - Apartamento Perdizes SP', '20151027185005_03_Casal IMG_0725-1 - Cópia.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 5, 'Dormitório Jovem I - Apartamento Perdizes SP', '20151028165936_04_Solteiro1 IMG_0664-1 - Cópia.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 7, 'Dormitório Jovem II - Apartamento Perdizes SP', '20151028172101_05_Solteiro2 IMG_0693-002.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 9, 'Dormitório Jovem III - Apartamento Perdizes ', '20151027195226_06_Solteiro3 IMG_0756-1.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projetos_imagens`
--

CREATE TABLE `projetos_imagens` (
  `id` int(10) UNSIGNED NOT NULL,
  `projetos_id` int(11) NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `largura` int(11) DEFAULT NULL,
  `altura` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `projetos_imagens`
--

INSERT INTO `projetos_imagens` (`id`, `projetos_id`, `ordem`, `imagem`, `largura`, `altura`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '20150602161537_annaparisi-7995.jpg', 3000, 2003, '2015-06-02 19:15:43', '2016-01-27 16:02:29'),
(14, 1, 3, '20150804140711_annaparisi-8016.jpg', 3000, 4384, '2015-08-04 17:07:42', '2016-01-27 16:02:29'),
(19, 1, 0, '20150804145323_annaparisi-8003.jpg', 3000, 3902, '2015-08-04 17:53:43', '2016-01-27 16:02:29'),
(36, 3, 7, '20150810202713_JJ 001.jpg', 2304, 3072, '2015-08-10 23:27:15', '2016-01-27 16:02:29'),
(37, 3, 8, '20150811134410_JJ 004.jpg', 3000, 2250, '2015-08-11 16:44:16', '2016-01-27 16:02:29'),
(38, 3, 6, '20150811134449_JJ 006.jpg', 2304, 3072, '2015-08-11 16:44:53', '2016-01-27 16:02:29'),
(40, 3, 0, '20150811135255_JJ 025.jpg', 2304, 3072, '2015-08-11 16:52:58', '2016-01-27 16:02:30'),
(42, 3, 11, '20150812173413_JJ 142.jpg', 1944, 2592, '2015-08-12 20:34:16', '2016-01-27 16:02:30'),
(43, 3, 5, '20150812174450_JJ 149.jpg', 3000, 2250, '2015-08-12 20:44:54', '2016-01-27 16:02:30'),
(44, 3, 2, '20150812174539_JJ 155.jpg', 2304, 3072, '2015-08-12 20:45:42', '2016-01-27 16:02:30'),
(45, 3, 4, '20150812174629_JJ 015.jpg', 2304, 3072, '2015-08-12 20:46:31', '2016-01-27 16:02:30'),
(46, 3, 3, '20150812174748_JJ 020.jpg', 2304, 3072, '2015-08-12 20:47:51', '2016-01-27 16:02:30'),
(47, 3, 1, '20150812185120_JJ 157.jpg', 3000, 2250, '2015-08-12 21:51:23', '2016-01-27 16:02:30'),
(55, 5, 3, '20150813145645__FJR3152.JPG', 3000, 4494, '2015-08-13 17:56:55', '2016-01-27 16:02:30'),
(59, 6, 1, '20150813202259_IMG_6479.JPG', 1936, 2592, '2015-08-13 23:23:01', '2016-01-27 16:02:30'),
(60, 6, 3, '20150813202521_3291copia.jpg', 1936, 2592, '2015-08-13 23:25:23', '2016-01-27 16:02:30'),
(61, 6, 2, '20150813202653_IMG_6470.JPG', 1936, 2592, '2015-08-13 23:26:55', '2016-01-27 16:02:30'),
(63, 6, 5, '20150813203000_3289.JPG', 1936, 2592, '2015-08-13 23:30:02', '2016-01-27 16:02:30'),
(69, 2, 5, '20150814141249__FJR3125.JPG', 3000, 4494, '2015-08-14 17:12:59', '2016-01-27 16:02:30'),
(72, 2, 3, '20150814143821__FJR3103 2.jpg', 3000, 2003, '2015-08-14 17:38:27', '2016-01-27 16:02:30'),
(76, 2, 1, '20150819213028__FJR3923.JPG', 3000, 1439, '2015-08-20 00:30:32', '2016-01-27 16:02:30'),
(77, 7, 0, '20150911191338_AP2.JPG', 804, 1049, '2015-09-11 22:13:38', '2016-01-27 16:02:30'),
(79, 7, 8, '20150911191609_AP3.JPG', 796, 1049, '2015-09-11 22:16:09', '2016-01-27 16:02:30'),
(80, 7, 1, '20150911193337_AP4.JPG', 791, 1049, '2015-09-11 22:33:38', '2016-01-27 16:02:30'),
(82, 7, 9, '20150911193422_AP6.JPG', 790, 1049, '2015-09-11 22:34:23', '2016-01-27 16:02:30'),
(83, 7, 7, '20150911194112_AP7.JPG', 806, 1048, '2015-09-11 22:41:13', '2016-01-27 16:02:30'),
(85, 7, 5, '20150911194143_AP8.JPG', 789, 1049, '2015-09-11 22:41:44', '2016-01-27 16:02:30'),
(86, 7, 4, '20150911194206_AP10.JPG', 794, 1049, '2015-09-11 22:42:08', '2016-01-27 16:02:30'),
(87, 7, 3, '20150911194309_AP11.JPG', 788, 1049, '2015-09-11 22:43:10', '2016-01-27 16:02:31'),
(88, 7, 2, '20150911194334_AP12.JPG', 789, 1047, '2015-09-11 22:43:35', '2016-01-27 16:02:31'),
(93, 8, 3, '20150914141911_AP18.JPG', 749, 1049, '2015-09-14 17:19:11', '2016-01-27 16:02:31'),
(94, 8, 4, '20150914142003_AP19.JPG', 753, 1049, '2015-09-14 17:20:03', '2016-01-27 16:02:31'),
(96, 8, 2, '20150914142029_AP21.JPG', 756, 1049, '2015-09-14 17:20:29', '2016-01-27 16:02:31'),
(100, 9, 1, '20150914144611_AP14.JPG', 803, 1049, '2015-09-14 17:46:11', '2016-01-27 16:02:31'),
(101, 9, 3, '20150914144636_AP17.JPG', 796, 1049, '2015-09-14 17:46:37', '2016-01-27 16:02:31'),
(102, 9, 2, '20150914144834_AP16.JPG', 718, 1049, '2015-09-14 17:48:35', '2016-01-27 16:02:31'),
(106, 10, 4, '20150915152101_annaparisi-8035.jpg', 3000, 4231, '2015-09-15 18:21:09', '2016-01-27 16:02:31'),
(107, 11, 1, '20150916141002_IMG_7467_Julia_Ribeiro.jpg', 3000, 2000, '2015-09-16 17:10:11', '2016-01-27 16:02:31'),
(108, 11, 2, '20150916141337_IMG_7468_Julia_Ribeiro.jpg', 3000, 2000, '2015-09-16 17:13:47', '2016-01-27 16:02:31'),
(109, 11, 3, '20150916153202_IMG_7478_Julia_Ribeiro.jpg', 3000, 2000, '2015-09-16 18:32:08', '2016-01-27 16:02:31'),
(110, 1, 1, '20150916153650_annaparisi-8011.jpg', 3000, 2127, '2015-09-16 18:36:58', '2016-01-27 16:02:31'),
(111, 1, 7, '20150916154815_annaparisi-8010.jpg', 3000, 1973, '2015-09-16 18:48:23', '2016-01-27 16:02:31'),
(112, 1, 5, '20150916155959_annaparisi-8031.jpg', 3000, 2003, '2015-09-16 19:00:07', '2016-01-27 16:02:31'),
(114, 5, 0, '20150921200529__FJR3146.JPG', 3000, 2003, '2015-09-21 23:05:36', '2016-01-27 16:02:31'),
(117, 12, 10, '20151019131632_IMG_0125.jpg', 2141, 3218, '2015-10-19 15:16:35', '2016-01-27 16:02:31'),
(119, 12, 7, '20151019135823_IMG_0114.jpg', 2556, 3834, '2015-10-19 15:58:27', '2016-01-27 16:02:31'),
(121, 12, 8, '20151019150223_IMG_0138.jpg', 2595, 3891, '2015-10-19 17:02:28', '2016-01-27 16:02:31'),
(123, 13, 4, '20151019161539_IMG_0038.jpg', 3000, 2143, '2015-10-19 18:15:44', '2016-01-27 16:02:31'),
(124, 13, 6, '20151019161724_IMG_0031.jpg', 3000, 2000, '2015-10-19 18:17:32', '2016-01-27 16:02:31'),
(126, 13, 5, '20151019162433_IMG_9961.jpg', 2538, 3810, '2015-10-19 18:24:38', '2016-01-27 16:02:31'),
(127, 13, 7, '20151019165348_IMG_9994.jpg', 2592, 3888, '2015-10-19 18:53:51', '2016-01-27 16:02:32'),
(129, 13, 8, '20151019165728_IMG_0065.jpg', 2592, 3888, '2015-10-19 18:57:32', '2016-01-27 16:02:32'),
(132, 12, 9, '20151019171732_IMG_0119.jpg', 3000, 2000, '2015-10-19 19:17:41', '2016-01-27 16:02:32'),
(133, 10, 0, '20151019184637_IMG_0643.jpg', 1000, 667, '2015-10-19 20:46:38', '2016-01-27 16:02:32'),
(134, 10, 1, '20151019185947_IMG_0640.jpg', 667, 1000, '2015-10-19 20:59:48', '2016-01-27 16:02:32'),
(135, 10, 3, '20151019191317_IMG_0659.jpg', 1000, 669, '2015-10-19 21:13:19', '2016-01-27 16:02:32'),
(136, 10, 2, '20151019191619_IMG_0663.jpg', 666, 1000, '2015-10-19 21:16:20', '2016-01-27 16:02:32'),
(137, 1, 6, '20151020135326_annaparisi-8008.jpg', 3000, 2003, '2015-10-20 15:53:41', '2016-01-27 16:02:32'),
(140, 11, 5, '20151020170901_IMG_7447_Julia_Ribeiro.jpg', 3000, 4500, '2015-10-20 19:09:20', '2016-01-27 16:02:32'),
(141, 11, 4, '20151020171242_IMG_7453_Julia_Ribeiro.jpg', 3000, 2000, '2015-10-20 19:12:55', '2016-01-27 16:02:32'),
(148, 10, 5, '20151020185052_annaparisi-8041.jpg', 3000, 4494, '2015-10-20 20:51:30', '2016-01-27 16:02:32'),
(150, 10, 6, '20151020191302_annaparisi-8042 (2).jpg', 3000, 4494, '2015-10-20 21:13:13', '2016-01-27 16:02:32'),
(152, 10, 7, '20151021132644_IMG_0670.jpg', 667, 1000, '2015-10-21 15:26:45', '2016-01-27 16:02:32'),
(154, 2, 0, '20151021134524__FJR3935.JPG', 3000, 3997, '2015-10-21 15:45:34', '2016-01-27 16:02:32'),
(155, 9, 0, '20151021190256_Fachada interna depois.jpg', 3000, 1248, '2015-10-21 21:03:00', '2016-01-27 16:02:32'),
(156, 15, 0, '20151022130434_annaparisi-7990.jpg', 3000, 4242, '2015-10-22 15:04:47', '2016-01-27 16:02:32'),
(157, 15, 0, '20151022130906_annaparisi-7991 (2).jpg', 3000, 4670, '2015-10-22 15:09:26', '2016-01-27 16:02:32'),
(159, 14, 0, '20151022133306_annaparisi-8065 (2).jpg', 3000, 2003, '2015-10-22 15:33:21', '2016-01-27 16:02:33'),
(160, 16, 0, '20151022133745_annaparisi-8044.jpg', 3000, 4415, '2015-10-22 15:38:05', '2016-01-27 16:02:33'),
(161, 16, 0, '20151022133952_annaparisi-8045 (2).jpg', 3000, 2003, '2015-10-22 15:40:07', '2016-01-27 16:02:33'),
(162, 16, 0, '20151022134404_annaparisi-8048.jpg', 3000, 1995, '2015-10-22 15:44:18', '2016-01-27 16:02:33'),
(163, 16, 0, '20151022134603_annaparisi-8049 (2).jpg', 3000, 2003, '2015-10-22 15:46:15', '2016-01-27 16:02:33'),
(164, 17, 0, '20151022135855_annaparisi-8060.jpg', 3000, 3838, '2015-10-22 15:59:13', '2016-01-27 16:02:33'),
(165, 17, 0, '20151022140118_annaparisi-8054.jpg', 2596, 3939, '2015-10-22 16:01:24', '2016-01-27 16:02:33'),
(166, 17, 0, '20151022140415_annaparisi-8056.jpg', 3000, 2003, '2015-10-22 16:04:25', '2016-01-27 16:02:33'),
(167, 17, 0, '20151022141219_annaparisi-8059.jpg', 3000, 2003, '2015-10-22 16:12:27', '2016-01-27 16:02:33'),
(168, 18, 1, '20151022171048_IMG_7538_Julia_Ribeiro.jpg', 3000, 4500, '2015-10-22 19:12:02', '2016-01-27 16:02:33'),
(169, 18, 4, '20151022171735_IMG_7525_Julia_Ribeiro.jpg', 3000, 1999, '2015-10-22 19:17:46', '2016-01-27 16:02:33'),
(170, 18, 6, '20151022171950_IMG_7528_Julia_Ribeiro.jpg', 3000, 2000, '2015-10-22 19:20:30', '2016-01-27 16:02:33'),
(172, 18, 7, '20151022173551_IMG_7544_Julia_Ribeiro.jpg', 3000, 2000, '2015-10-22 19:36:08', '2016-01-27 16:02:33'),
(173, 18, 3, '20151022174000_IMG_7518_Julia_Ribeiro.jpg', 3000, 2000, '2015-10-22 19:40:39', '2016-01-27 16:02:33'),
(174, 18, 2, '20151022175736_IMG_7520_Julia_Ribeiro.jpg', 3000, 2000, '2015-10-22 19:57:56', '2016-01-27 16:02:33'),
(175, 18, 0, '20151022180204_IMG_7537_Julia_Ribeiro.jpg', 2669, 3517, '2015-10-22 20:02:13', '2016-01-27 16:02:33'),
(176, 18, 5, '20151022180548_IMG_7526_Julia_Ribeiro.jpg', 3000, 2000, '2015-10-22 20:06:02', '2016-01-27 16:02:34'),
(177, 13, 0, '20151022182911_IMG_0017.jpg', 2558, 3789, '2015-10-22 20:29:37', '2016-01-27 16:02:34'),
(178, 11, 0, '20151022185123_IMG_7473_Julia_Ribeiro.jpg', 3000, 4500, '2015-10-22 20:51:39', '2016-01-27 16:02:34'),
(180, 1, 4, '20151022190109_annaparisi-8013 (2).jpg', 3000, 2003, '2015-10-22 21:01:40', '2016-01-27 16:02:34'),
(181, 6, 0, '20151022192646_IMG_6477 - Cópia - Cópia.JPG', 1257, 1852, '2015-10-22 21:26:49', '2016-01-27 16:02:34'),
(182, 19, 0, '20151023132704_amparisi 061 - Cópia.jpg', 2304, 3072, '2015-10-23 15:27:14', '2016-01-27 16:02:34'),
(183, 19, 0, '20151023133242_amparisi 018 - Cópia.jpg', 2458, 2304, '2015-10-23 15:32:47', '2016-01-27 16:02:34'),
(184, 19, 0, '20151023133434_amparisi 024 - Cópia.jpg', 3000, 2250, '2015-10-23 15:34:41', '2016-01-27 16:02:34'),
(185, 20, 1, '20151023134719_amparisi 030 - Cópia.jpg', 2304, 3072, '2015-10-23 15:47:24', '2016-01-27 16:02:34'),
(186, 20, 4, '20151023134824_amparisi 060 - Cópia.jpg', 2304, 3072, '2015-10-23 15:48:35', '2016-01-27 16:02:34'),
(187, 20, 3, '20151023134931_amparisi 035 - Cópia.jpg', 3000, 2250, '2015-10-23 15:49:38', '2016-01-27 16:02:34'),
(189, 20, 2, '20151023135058_amparisi 034 - Cópia.jpg', 3000, 2250, '2015-10-23 15:51:13', '2016-01-27 16:02:34'),
(191, 20, 0, '20151023135516_amparisi 033 - Cópia.jpg', 2304, 3072, '2015-10-23 15:55:27', '2016-01-27 16:02:34'),
(192, 5, 2, '20151023141829__FJR3151.JPG', 3000, 2003, '2015-10-23 16:18:38', '2016-01-27 16:02:34'),
(193, 5, 1, '20151023142021__FJR3149.JPG', 3000, 4493, '2015-10-23 16:20:41', '2016-01-27 16:02:34'),
(194, 21, 0, '20151023144037__FJR3128 (2).JPG', 3000, 4494, '2015-10-23 16:40:51', '2016-01-27 16:02:34'),
(195, 21, 0, '20151023144431__FJR3158.JPG', 3000, 4494, '2015-10-23 16:44:42', '2016-01-27 16:02:35'),
(196, 22, 0, '20151023171422__FJR3144 (2).JPG', 3000, 4180, '2015-10-23 19:14:32', '2016-01-27 16:02:35'),
(197, 22, 0, '20151023171604__FJR3163.JPG', 3000, 4494, '2015-10-23 19:16:17', '2016-01-27 16:02:35'),
(198, 22, 0, '20151023171925__FJR3164.JPG', 3000, 2003, '2015-10-23 19:19:45', '2016-01-27 16:02:35'),
(203, 13, 0, '20151023174724_Panorama4-2.jpg', 3000, 2140, '2015-10-23 19:47:35', '2016-01-27 16:02:35'),
(204, 12, 6, '20151023183318_IMG_0141.jpg', 2566, 3849, '2015-10-23 20:33:23', '2016-01-27 16:02:35'),
(205, 12, 5, '20151023183448_IMG_0144.jpg', 2217, 3390, '2015-10-23 20:34:55', '2016-01-27 16:02:35'),
(206, 12, 11, '20151023183606_IMG_0151.jpg', 2526, 3789, '2015-10-23 20:36:12', '2016-01-27 16:02:35'),
(207, 18, 8, '20151023184031_IMG_7534_Julia_Ribeiro.jpg', 3000, 4501, '2015-10-23 20:40:47', '2016-01-27 16:02:35'),
(208, 11, 6, '20151023184422_IMG_7462_Julia_Ribeiro.jpg', 3000, 4500, '2015-10-23 20:44:50', '2016-01-27 16:02:35'),
(209, 2, 4, '20151023185342__FJR3930.JPG', 3000, 2003, '2015-10-23 20:54:01', '2016-01-27 16:02:35'),
(210, 8, 0, '20151023185655_tt 046.jpg', 3000, 2250, '2015-10-23 20:57:02', '2016-01-27 16:02:35'),
(211, 8, 1, '20151023185737_tt 048.jpg', 3000, 2250, '2015-10-23 20:57:42', '2016-01-27 16:02:35'),
(212, 14, 0, '20151023185948_055.JPG', 640, 640, '2015-10-23 20:59:49', '2016-01-27 16:02:35'),
(213, 23, 0, '20151027181028_01_Estar IMG_0827-1.jpg', 3000, 2000, '2015-10-27 20:10:35', '2016-01-27 16:08:09'),
(214, 23, 2, '20151027181153_01_Estar IMG_0806-1.jpg', 3000, 2096, '2015-10-27 20:11:59', '2016-01-27 16:08:09'),
(216, 23, 3, '20151027181731_01_Estar IMG_0833-1.jpg', 3000, 1884, '2015-10-27 20:17:35', '2016-01-27 16:08:09'),
(217, 23, 5, '20151027181943_01_Jantar IMG_0776-1.jpg', 3000, 2027, '2015-10-27 20:19:49', '2016-01-27 16:08:09'),
(218, 23, 4, '20151027182121_01_Jantar IMG_0828-1.jpg', 3000, 2000, '2015-10-27 20:21:28', '2016-01-27 16:08:09'),
(220, 23, 9, '20151027182544_02_Terrac¦ºo IMG_0813-1.jpg', 3000, 2000, '2015-10-27 20:25:48', '2016-01-27 16:08:09'),
(221, 23, 11, '20151027182904_02_Terrac¦ºo IMG_0814-1.jpg', 3000, 2000, '2015-10-27 20:29:09', '2016-01-27 16:08:09'),
(222, 23, 12, '20151027183136_02_Terrac¦ºo IMG_0835-1.jpg', 3000, 2020, '2015-10-27 20:31:41', '2016-01-27 16:08:09'),
(225, 24, 1, '20151027184357_03_Casal IMG_0725-1.jpg', 3000, 4500, '2015-10-27 20:44:07', '2016-01-27 16:08:09'),
(226, 25, 0, '20151027185629_04_Solteiro1 IMG_0666-1.jpg', 3000, 2000, '2015-10-27 20:56:35', '2016-01-27 16:08:09'),
(227, 25, 0, '20151027185834_04_Solteiro1 IMG_0660-1.jpg', 3000, 2442, '2015-10-27 20:58:40', '2016-01-27 16:08:09'),
(228, 25, 0, '20151027191741_04_Solteiro1 IMG_0675-1.jpg', 3000, 4500, '2015-10-27 21:17:49', '2016-01-27 16:08:09'),
(229, 25, 0, '20151027191921_04_Solteiro1 IMG_0664-1.jpg', 3000, 4209, '2015-10-27 21:19:27', '2016-01-27 16:08:09'),
(230, 26, 0, '20151027193503_05_Solteiro2 IMG_0696-1.jpg', 3000, 2000, '2015-10-27 21:35:08', '2016-01-27 16:08:09'),
(231, 26, 0, '20151027193741_05_Solteiro2 IMG_0689-1.jpg', 3000, 2000, '2015-10-27 21:37:47', '2016-01-27 16:08:09'),
(232, 26, 0, '20151027194027_05_Solteiro2 IMG_0707-1.jpg', 3000, 4500, '2015-10-27 21:40:34', '2016-01-27 16:08:10'),
(234, 26, 0, '20151027194802_05_Solteiro2 IMG_0704-1.jpg', 3000, 2000, '2015-10-27 21:48:07', '2016-01-27 16:08:10'),
(235, 27, 0, '20151027195925_06_Solteiro3 IMG_0736-1.jpg', 3000, 2000, '2015-10-27 21:59:30', '2016-01-27 16:08:10'),
(236, 27, 0, '20151027200154_06_Solteiro3 IMG_0745-1.jpg', 3000, 2061, '2015-10-27 22:01:58', '2016-01-27 16:08:10'),
(237, 27, 0, '20151027200317_06_Solteiro3 IMG_0749-1.jpg', 3000, 4500, '2015-10-27 22:03:23', '2016-01-27 16:08:10'),
(238, 27, 0, '20151027200416_06_Solteiro3 IMG_0752-1.jpg', 3000, 4500, '2015-10-27 22:04:23', '2016-01-27 16:08:10'),
(239, 27, 0, '20151027200757_06_Solteiro3 IMG_0745-1.jpg', 3000, 2061, '2015-10-27 22:08:01', '2016-01-27 16:08:10'),
(240, 27, 0, '20151027201350_06_Solteiro3 IMG_0738-1.jpg', 3000, 2000, '2015-10-27 22:13:55', '2016-01-27 16:08:10'),
(241, 12, 3, '20151028144132_IMG_0072-2-2.jpg', 3000, 2002, '2015-10-28 16:41:37', '2016-01-27 16:08:10'),
(242, 12, 0, '20151028144213_Untitled_Panorama1-2-2.jpg', 3000, 1306, '2015-10-28 16:42:15', '2016-01-27 16:08:10'),
(244, 12, 1, '20151028144752_IMG_0096-2.jpg', 3000, 2290, '2015-10-28 16:47:58', '2016-01-27 16:08:10'),
(245, 12, 2, '20151028163844_IMG_0098.jpg', 2592, 3888, '2015-10-28 18:38:47', '2016-01-27 16:08:10'),
(246, 12, 4, '20151028164214_IMG_0163-3.jpg', 2432, 3784, '2015-10-28 18:42:18', '2016-01-27 16:08:10'),
(248, 23, 1, '20151028170630_01_Estar IMG_0791-1 - Cópia.jpg', 3000, 2101, '2015-10-28 19:06:35', '2016-01-27 16:08:10'),
(249, 23, 6, '20151028171211_01_Jantar IMG_0785-1.jpg', 3000, 4499, '2015-10-28 19:12:20', '2016-01-27 16:08:10'),
(250, 23, 7, '20151028171412_01_Jantar IMG_0773-1.jpg', 3000, 2051, '2015-10-28 19:14:20', '2016-01-27 16:08:10'),
(251, 23, 8, '20151028171651_02_Terrac¦ºo IMG_0767-1.jpg', 3000, 4544, '2015-10-28 19:16:58', '2016-01-27 16:08:11'),
(252, 26, 0, '20151028171905_05_Solteiro2 IMG_0693-002.jpg', 3000, 3652, '2015-10-28 19:19:13', '2016-01-27 16:08:11'),
(253, 23, 10, '20151028174825_02_Terrac¦ºo IMG_0811-1.jpg', 3000, 4500, '2015-10-28 19:48:33', '2016-01-27 16:08:11'),
(254, 23, 13, '20151028180819_02_Hall IMG_0815-1.jpg', 3000, 4500, '2015-10-28 20:08:30', '2016-01-27 16:08:11'),
(255, 23, 14, '20151028181008_02_Hall IMG_0817-1.jpg', 3000, 4500, '2015-10-28 20:10:15', '2016-01-27 16:08:11'),
(256, 24, 0, '20151029171711_03_Casal IMG_0718-1.jpg', 3000, 2383, '2015-10-29 19:17:17', '2016-01-27 16:08:11'),
(257, 24, 2, '20151029171939_03_Casal IMG_0733-1.jpg', 3000, 2000, '2015-10-29 19:19:44', '2016-01-27 16:08:11');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `email`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'contato@trupe.net', 'trupe', '$2y$10$9/E/llTAPQw.rzGw2JvWp./lxB.QAaRJSc8OprwuiERrx/DgPeAhO', 'xcF95Ah8cwEcfqbXCAqcSp8lz6hnKFtQKd97EZyo1fTUfE8CamETVBd6VTAD', '0000-00-00 00:00:00', '2015-06-08 17:03:28'),
(2, 'contato@annaparisi.com.br', 'anna', '$2y$10$j1M0jmIop4tT40DzSFp8iuN7q3Wp73HAZYZ4W6CQXnlpw.48CnD4G', 'yaJeRNWWuUqbfzAfzXtfOWU7kZVkYMPtOk3nhirf1SLItGpiEVU6QnQy8FED', '2015-06-08 17:03:23', '2015-10-29 19:48:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imprensa`
--
ALTER TABLE `imprensa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imprensa_imagens`
--
ALTER TABLE `imprensa_imagens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mostras`
--
ALTER TABLE `mostras`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mostras_imagens`
--
ALTER TABLE `mostras_imagens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `perfil`
--
ALTER TABLE `perfil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projetos`
--
ALTER TABLE `projetos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projetos_imagens`
--
ALTER TABLE `projetos_imagens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `usuarios_email_unique` (`email`),
  ADD UNIQUE KEY `usuarios_username_unique` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `imprensa`
--
ALTER TABLE `imprensa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `imprensa_imagens`
--
ALTER TABLE `imprensa_imagens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `mostras`
--
ALTER TABLE `mostras`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `mostras_imagens`
--
ALTER TABLE `mostras_imagens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `perfil`
--
ALTER TABLE `perfil`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `projetos`
--
ALTER TABLE `projetos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `projetos_imagens`
--
ALTER TABLE `projetos_imagens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;
--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
